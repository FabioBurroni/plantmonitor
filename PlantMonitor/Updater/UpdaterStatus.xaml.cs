﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using ExtendedUtilities.Wpf;
using NAppUpdate.Framework;
using NAppUpdate.Framework.Common;
using NAppUpdate.Framework.Sources;
using NAppUpdate.Framework.Tasks;

namespace PlantMonitor.Updater
{
 
  /// <summary>
  /// Interaction logic for UpdaterStatus.xaml
  /// </summary>
  public partial class UpdaterStatus : Window
  {
    private static readonly UpdateManager UpdManager = UpdateManager.Instance;
    private static IUpdateSource _source;

    public bool InstallWithoutUserConfirmation { get; set; }

    public UpdaterStatus()
    {
      InitializeComponent();
      Loaded += delegate { SetChangelog(); };

      ContentRendered += delegate
      {
        if (InstallWithoutUserConfirmation)
          PrepareUpdates(InstallWithoutUserConfirmation);
      };
    }

    #region Command

    private ICommand _updateCommand;

    public ICommand UpdateCommand
    {
      get { return _updateCommand ?? (_updateCommand = new RelayCommand<bool>(PrepareUpdates)); }
    }


    #endregion
    private void PrepareUpdates(bool update)
    {
      if (!update)
      {
        Close();
        return;
      }

      BtnYes.IsEnabled = false;
      BtnNo.IsEnabled = false;

      UpdManager.ReportProgress += delegate (UpdateProgressInfo status)
      {
        var dispatcher = Application.Current.Dispatcher;

        dispatcher.BeginInvoke((Action)(() =>
        {
          var details = status.Message;
          var overview = string.Format("Phase: {0}, executing task #{1}: {2}", UpdateManager.Instance.State, status.TaskId, status.TaskDescription);

          SetStatus(details, overview);

          ProgressBar.Value = status.Percentage;
        }));
      };

      ApplyUpdates();
    }

    private void ApplyUpdates()
    {
      UpdManager.BeginPrepareUpdates(asyncResult =>
      {
        ((UpdateProcessAsyncResult)asyncResult).EndInvoke();

        // ApplyUpdates is a synchronous method by design. Make sure to save all user work before calling
        // it as it might restart your application
        // get out of the way so the console window isn't obstructed
        Dispatcher d = Application.Current.Dispatcher;
        d.BeginInvoke(new Action(Hide));
        try
        {
          UpdManager.ApplyUpdates(true);
          d.BeginInvoke(new Action(Close));
        }
        catch
        {
          d.BeginInvoke(new Action(Show));
          // this.WindowState = WindowState.Normal;
          MessageBox.Show("An error occurred while trying to install software updates");
        }

        UpdManager.CleanUp();
        d.BeginInvoke(new Action(Close));

        Action close = Close;

        if (Dispatcher.CheckAccess())
          close();
        else
          Dispatcher.Invoke(close);
      }, null);
    }

    private void SetChangelog()
    {
      var tasks = GetUpdateTaskInfo();

      var sb = new StringBuilder();

      foreach (var i in Enumerable.Range(0, tasks.Count))
      {
        sb.AppendFormat("File: {0}, Description: {1}, Version: {2}", tasks[i].FileName, tasks[i].FileDescription, tasks[i].FileVersion);

        if (i + 1 < tasks.Count)
          sb.Append("\r\n");
      }

      TextBlockChangelog.Text = sb.ToString();
    }

    private void SetStatus(string details, string overview)
    {
      TextBlockStatusDetails.Text = details;
      TextBlockStatusOverview.Text = overview;
    }

    #region Metodi statici

    private static void SetSource(string updateSource)
    {
      if (UpdManager.UpdateSource == null)
      {
        if (string.IsNullOrEmpty(updateSource))
          throw new ArgumentNullException("updateSource");

        _source = new SimpleWebSource(updateSource);
        UpdManager.UpdateSource = _source;
      }
    }

    /// <summary>
    /// Controlla gli aggiornamenti all'indirizzo specificato nella configurazione e chiede all'utente se vuole aggiornare.
    /// </summary>
    /// <param name="updateSource">Url del feed xml</param>
    public static void CheckForUpdates(string updateSource)
    {
      CheckForUpdates(false, updateSource);
    }

    /// <summary>
    /// Controlla gli aggiornamenti all'indirizzo specificato nella configurazione. Si può scegliere se far confermare all'utente l'aggiornamento
    /// oppure obbligare l'aggiornamento
    /// </summary>
    /// <param name="installWithoutUserConfirm">Se true, l'aggiornamento è obbligatorio e l'utente non ha scelta</param>
    /// <param name="updateSource">Url del feed xml</param>
    public static void CheckForUpdates(bool installWithoutUserConfirm, string updateSource)
    {
      SetSource(updateSource);

      // If you don't call this method, the updater.exe will continually attempt to connect the named pipe and get stuck.
      // Therefore you should always implement this method call.
      UpdManager.ReinstateIfRestarted();

      UpdManager.CleanUp();

      try
      {
        UpdManager.BeginCheckForUpdates(asyncResult =>
        {
          try
          {
            ((UpdateProcessAsyncResult)asyncResult).EndInvoke();
            if (UpdManager.UpdatesAvailable > 0)
            {
              var dispatcher = Application.Current.Dispatcher;

              dispatcher.BeginInvoke((Action)(() =>
              {
                var us = new UpdaterStatus();
                us.ShowDialog(installWithoutUserConfirm);
              }));
            }
          }
          catch(Exception ex)
          {
            //Errore di connessione al server
          }
        }, null);
      }
      catch (Exception ex)
      {
        MessageBox.Show("Error checking updates...");
      }
    }

    private bool? ShowDialog(bool installWithoutUserConfirm)
    {
      InstallWithoutUserConfirmation = installWithoutUserConfirm;

      return ShowDialog();
    }

    private static IList<UpdateTaskInfo> GetUpdateTaskInfo()
    {
      var taskListInfo = new List<UpdateTaskInfo>();

      foreach (var task in UpdManager.Tasks)
      {
        var fileTask = task as FileUpdateTask;
        if (fileTask == null) continue;

        taskListInfo.Add(new UpdateTaskInfo { FileDescription = fileTask.Description, FileName = fileTask.LocalPath });
      }

      return taskListInfo;
    }

    #endregion

  }
  public class UpdateTaskInfo
  {
    // TaskDescription?
    public string FileDescription { get; set; }
    public string FileName { get; set; }
    public string FileVersion { get; set; }
    public long? FileSize { get; set; }
    public DateTime? FileDate { get; set; }
  }
}
