﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows;

namespace View.Common.Converter
{
  public class StringEmptyOrNullToVisibilityConverter : IValueConverter
  {
    private Visibility _emptyOrNullValue = Visibility.Collapsed;

    public Visibility EmptyOrNullValue
    {
      get { return _emptyOrNullValue; }
      set { _emptyOrNullValue = value; }
    }

    private Visibility _validValue = Visibility.Visible;
    public Visibility ValidValue
    {
      get { return _validValue; }
      set {_validValue = value; }
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value is string)
      {
        if (!string.IsNullOrEmpty((string)value))
          return ValidValue;
      }
      return EmptyOrNullValue;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
