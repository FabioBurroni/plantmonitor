﻿using System;
using System.Collections.Generic;
using XmlCommunicationManager.XmlClient.Event;
using Utilities.Extensions;
using Model.Common;
using System.Windows.Controls;
using Localization;
using System.ComponentModel;
using Utilities.Events;

namespace View.Common
{
  public class CtrlBase: CtrlFunction, INotifyPropertyChanged
  {
    #region Notify property changed

    public event PropertyChangedEventHandler PropertyChanged;

    // This method is called by the Set accessor of each property.
    // The CallerMemberName attribute that is applied to the optional propertyName
    // parameter causes the property name of the caller to be substituted as an argument.
    protected void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string propertyName = "")
    {
      PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion Notify property changed

   
    #region Constructor

    public CtrlBase()
    {
      // Check for design mode. 
      if (DesignTimeHelper.IsInDesignMode)
        return;

      InitCommandManager();
      SubscribeXmlClient();

      SubscribeCultureManager();
    }

    #endregion

    #region Override

    public override bool IsSelected { get; set; }

    public override void Closing()
    {
      UnsubscribeCultureManager();
      UnsubscribeXmlClient();
    }

    #endregion

    #region Protected Properties

    protected Context Context { get; } = Context.Instance;
    protected CommandManager CommandManager { get; private set; }
    protected CultureManager CultureManager { get; } = CultureManager.Instance;


    #endregion

    #region CommandManager

    private void InitCommandManager()
    {
      CommandManager = new CommandManager(Context.XmlClient);
    }

    #endregion

    #region XmlClient

    private void SubscribeXmlClient()
    {
      Context.XmlClient.OnRetrieveReplyFromServer += XmlClientOnRetrieveReplyFromServer;
      Context.XmlClient.OnException += XmlClientOnException;
    }

    private void UnsubscribeXmlClient()
    {
      Context.XmlClient.OnRetrieveReplyFromServer -= XmlClientOnRetrieveReplyFromServer;
      Context.XmlClient.OnException -= XmlClientOnException;
    }

    protected virtual void XmlClientOnException(object sender, ExceptionEventArgs e)
    {
      ManageXmlClientException(CommandManager, e);
    }

    protected virtual void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs e)
    {
      ManageXmlClientReply(CommandManager, e);
    }

    protected bool ManageXmlClientException(ICommandManager cm, ExceptionEventArgs e)
    {
      var cr = cm.Decode(e);
      if (cr == null)
        return false;

      Dispatch(cr.CommandName, cr.CommandParameters, cr.ResponseModel);

      return true;
    }

    protected bool ManageXmlClientReply(ICommandManager cm, MsgEventArgs e)
    {
      //pre-decodifico il comando per vedere se ho il metodo
      var cr = cm.PreDecode(e);
      if (cr == null)
        return false;

      //verifico se ho il metodo
      if (!GetType().ContainsMethod(cr.CommandName))
        return false;

      //decodifico il comando per i dati
      cr = cm.Decode(e);
      if (cr == null)
        return false;

      Dispatch(cr.CommandName, cr.CommandParameters, cr.ResponseModel);

      return true;
    }

    protected void Dispatch(string commandMethodName, IList<string> commandMethodParameters, IModel model)
    {
      Type[] types = { typeof(IList<string>), typeof(IModel) }; //esplicito i tipi perchè IModel potrebbe essere nullo
      object[] parameters = { commandMethodParameters, model };

      InvokeControlAction(this, delegate
      {
        //invoca il metodo che deve avere lo stesso nome del comando e firma precisa
        GetType().InvokeMethod(this, commandMethodName, types, parameters);
      });
    }

    #endregion
    
    #region CultureManager

    private void SubscribeCultureManager()
    {

      CultureManager.CultureChanged += CultureManagerOnCultureChanged;
    }

    private void UnsubscribeCultureManager()
    {
      CultureManager.CultureChanged -= CultureManagerOnCultureChanged;
    }

    protected virtual void CultureManagerOnCultureChanged(object sender, CultureChangedEventArgs e)
    {
      Translate();
    }

    protected virtual void Translate()
    {
    }

    #endregion
    #region Utilities

    /// <summary>
    /// Contesto attuale del controllo
    /// </summary>
    private static readonly System.Threading.SynchronizationContext SyncContext = System.Threading.SynchronizationContext.Current;

    /// <summary>
    /// Permette di eseguire il metodo <param name="action"/> della classe <param name="cont"/>
    /// invocandolo tramite thread principale così da evitare le eccezioni
    /// </summary>
    /// <typeparam name="T">Tipo della classe invocante</typeparam>
    /// <param name="cont">Classe invocante</param>
    /// <param name="action">Metodo da invocare</param>
    protected static void InvokeControlAction<T>(T cont, Action<T> action)
      where T : Control
    {
      //if (cont.InvokeRequired)
      //{
      //  cont.Invoke(new Action<T, Action<T>>(InvokeControlAction), cont, action);
      //}
      //else
      //{
      //  action(cont);
      //}

      //NOTE la vecchia implementazione prevedeva il classico InvokeRequired ma questo è valido solo se chi istanzia il controllo è una WinForms.
      //NOTE Per rendere compatibile il controllo anche con WPF, devo utilizzare il contesto.
      //NOTE ATTENZIONE: non è necessario utilizzare ISynchronizeInvoke.InvokeRequired o Dispatcher.CheckAccess, il controllo è interno al contesto stesso.

      if (SyncContext == null)
      {
        action(cont);
      }
      else
      {
        SyncContext.Post(o => action(cont), null);
      }
    }

    protected static bool IsWpfGuiThread()
    {
      //return System.Windows.Threading.Dispatcher.FromThread(System.Threading.Thread.CurrentThread) != null;
      return SyncContext is System.Windows.Threading.DispatcherSynchronizationContext;
    }

    protected static bool IsWinFormsGuiThread()
    {
      return true;
      //return SyncContext is System.Windows.Forms.WindowsFormsSynchronizationContext;
    }

    #endregion
  }
}
