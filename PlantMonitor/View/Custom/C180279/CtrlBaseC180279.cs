﻿using Configuration;
using Model.Custom.C180279;
using View.Common;
using Utilities.Extensions;
using XmlCommunicationManager.XmlClient.Event;

namespace View.Custom.C180279
{
  public class CtrlBaseC180279 : CtrlBase
  {
    public CtrlBaseC180279()
    {
      // Check for design mode. 
      if (DesignTimeHelper.IsInDesignMode)
        return;

      InitCommandManager();

    }

    #region Private Properties

    protected C180279_CommandManager CommandManagerC180279 { get; private set; }

    #endregion

    #region Init CommandManager

    private void InitCommandManager()
    {
      CommandManagerC180279 = new C180279_CommandManager(Context.XmlClient, ConfigurationManager.Parameters.CommandPreambleSetting.GetPreamble("custom"));
    }

    #endregion

    #region Override

    protected override void XmlClientOnException(object sender, ExceptionEventArgs e)
    {
      if (ManageXmlClientException(CommandManagerC180279, e))
        return;

      base.XmlClientOnException(sender, e);
    }

    protected override void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs e)
    {
      if (ManageXmlClientReply(CommandManagerC180279, e))
        return;

      base.XmlClientOnRetrieveReplyFromServer(sender, e);
    }

    #endregion

  }
}
