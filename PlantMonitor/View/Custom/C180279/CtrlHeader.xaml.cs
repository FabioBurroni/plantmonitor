﻿

namespace View.Custom.C180279
{
  /// <summary>
  /// Interaction logic for CtrlHeader.xaml
  /// </summary>
  public partial class CtrlHeader : CtrlBaseC180279
  {
    public CtrlHeader()
    {
      InitializeComponent();
    }

    #region TRADUZIONI
    protected override void Translate()
    {

    }

    #endregion TRADUZIONI

    private void CtrlBaseC200318_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Translate();
    }
  }
}
