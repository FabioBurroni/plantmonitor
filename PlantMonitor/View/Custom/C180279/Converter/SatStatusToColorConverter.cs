﻿using Model.Custom.C180279;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Custom.C180279.Converter

{
  public class SatStatusToColorConverter : IValueConverter
  {

    public bool IsForeground { get; set; } = true;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C180279_SatelliteStatusEnum)
      {
        var status = (C180279_SatelliteStatusEnum)value;

        switch (status)
        {
          case C180279_SatelliteStatusEnum.PLAY:
            //return IsForeground ? Brushes.White : Brushes.Green;
            return Brushes.Transparent;
          case C180279_SatelliteStatusEnum.STOP:
            return IsForeground ? Brushes.Yellow: Brushes.Red;
          case C180279_SatelliteStatusEnum.ERROR:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C180279_SatelliteStatusEnum.RESTORE:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C180279_SatelliteStatusEnum.RESET:
            return IsForeground ? Brushes.Yellow : Brushes.Red;
          case C180279_SatelliteStatusEnum.CHARGING:
            return IsForeground ? Brushes.Yellow : Brushes.Orange;
          case C180279_SatelliteStatusEnum.PARKING:
            //return IsForeground ? Brushes.White: Brushes.Blue;
            return Brushes.Transparent;
          case C180279_SatelliteStatusEnum.MANUAL:
            return IsForeground ? Brushes.White : Brushes.Gray;
          default:
            return IsForeground ? Brushes.White : Brushes.Gray;
        }
      }
        return IsForeground ? Brushes.White : Brushes.Gray;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
