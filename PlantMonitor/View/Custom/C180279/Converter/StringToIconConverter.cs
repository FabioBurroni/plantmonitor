﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace View.Custom.C180279.Converter
{
  public class StringToIconConverter : IValueConverter
  {
    private static ResourceDictionary iconDictionary;
    public ResourceDictionary IconDictionary
    {
      get
      {
        if (iconDictionary == null)
        {
          iconDictionary = new ResourceDictionary();
          iconDictionary.Source = new Uri("/PlantMonitor;component/View/Custom/C180279/C180279_ResourceDictionary.xaml", UriKind.RelativeOrAbsolute);
        }
        return iconDictionary;
      }
    }
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      string iconname = "default";
      if (value is string)
        iconname = (string)value;
      else
        iconname = value.ToString();
      if (iconname == null)
        return null;
      return IconDictionary[iconname.ToLower()];
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
