﻿using Model.Custom.C180279;
using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Custom.C180279.Converter

{
  public class SatStatusToStringConverter : IValueConverter
  {

    public bool IsForeground { get; set; } = true;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C180279_SatelliteStatusEnum)
      {
        var status = (C180279_SatelliteStatusEnum)value;

        switch (status)
        {
          case C180279_SatelliteStatusEnum.PLAY:
            return Localization.Localize.LocalizeDefaultString("PLAY");
          case C180279_SatelliteStatusEnum.STOP:
            return Localization.Localize.LocalizeDefaultString("STOP");
          case C180279_SatelliteStatusEnum.ERROR:
            return Localization.Localize.LocalizeDefaultString("ERROR");
          case C180279_SatelliteStatusEnum.RESTORE:
            return Localization.Localize.LocalizeDefaultString("RESTORE");
          case C180279_SatelliteStatusEnum.RESET:
            return Localization.Localize.LocalizeDefaultString("RESET");
          case C180279_SatelliteStatusEnum.CHARGING:
            return Localization.Localize.LocalizeDefaultString("CHARGING");
          case C180279_SatelliteStatusEnum.PARKING:            
            return Localization.Localize.LocalizeDefaultString("PARKING");
          case C180279_SatelliteStatusEnum.MANUAL:
            return Localization.Localize.LocalizeDefaultString("MANUAL");
          default:
            return "";
        }
      }
      return "";

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
