﻿using Model.Custom.C180279;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Custom.C180279.Converter

{
  public class SatAlarmServiceToColorConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C180279_AlarmServiceStatusEnum)
      {
        var status = (C180279_AlarmServiceStatusEnum)value;

        switch (status)
        {
          case C180279_AlarmServiceStatusEnum.UNDEFINED:
            return Brushes.Gray;
          case C180279_AlarmServiceStatusEnum.OK:
            return Brushes.Green;
          case C180279_AlarmServiceStatusEnum.ALARM:
            return Brushes.Red;
          case C180279_AlarmServiceStatusEnum.SERVICE:
           // return Brushes.Gold; Non si mostrano i service...
            return Brushes.Green;
          default:
            return Brushes.Gray;
        }
      }
        return Brushes.Gray;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
