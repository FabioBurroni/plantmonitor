﻿using Model.Custom.C180279;
using System;
using System.Timers;
using System.Windows;
using System.Windows.Controls;

namespace View.Custom.C180279.Machines
{
  /// <summary>
  /// Interaction logic for CtrlMaster.xaml
  /// </summary>
  public partial class CtrlMaster : UserControl
  {
    public Timer ChargeTimer = new Timer();

    public C180279_MasterStatus MasterStatus
    {
      get { return (C180279_MasterStatus)GetValue(MasterStatusProperty); }
      set { SetValue(MasterStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for MasterStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty MasterStatusProperty =
        DependencyProperty.Register("MasterStatus", typeof(C180279_MasterStatus), typeof(CtrlMaster), new PropertyMetadata(null));

    public CtrlMaster()
    {
      InitializeComponent();
    }

    private void UserControl_Loaded(object sender, RoutedEventArgs e)
    {
      ChargeTimer = new Timer();
      ChargeTimer.Interval = 3000;
      ChargeTimer.AutoReset = true;
      ChargeTimer.Elapsed += ChargeTimer_Elapsed; ;

      ChargeTimer.Start();

      ChargeLow.Visibility = System.Windows.Visibility.Visible;
      ChargeMedium.Visibility = System.Windows.Visibility.Hidden;
      ChargeFull.Visibility = System.Windows.Visibility.Hidden;

    }

    private void ChargeTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
      Application.Current.Dispatcher.BeginInvoke((Action)(() => ChangeChargeImage()));      
    }

    private void ChangeChargeImage()
    {
      if (ChargeLow.Visibility == System.Windows.Visibility.Visible)
      {
        ChargeLow.Visibility = System.Windows.Visibility.Hidden;
        ChargeMedium.Visibility = System.Windows.Visibility.Visible;
        ChargeFull.Visibility = System.Windows.Visibility.Hidden;
      }
      else if (ChargeMedium.Visibility == System.Windows.Visibility.Visible)
      {
        ChargeLow.Visibility = System.Windows.Visibility.Hidden;
        ChargeMedium.Visibility = System.Windows.Visibility.Hidden;
        ChargeFull.Visibility = System.Windows.Visibility.Visible;
      }
      else
      {
        ChargeLow.Visibility = System.Windows.Visibility.Visible;
        ChargeMedium.Visibility = System.Windows.Visibility.Hidden;
        ChargeFull.Visibility = System.Windows.Visibility.Hidden;
      }

    }

    private void UserControl_Unloaded(object sender, RoutedEventArgs e)
    {
      ChargeTimer.Stop();
      ChargeTimer.Dispose();

    }
  }
}
