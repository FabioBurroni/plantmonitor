﻿using Model.Custom.C180279;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace View.Custom.C180279.Machines
{
  /// <summary>
  /// Interaction logic for CtrlSlave.xaml
  /// </summary>
  public partial class CtrlSlave : UserControl
  {
    DispatcherTimer _dt = null;


    #region DP - SlaveStatus
    public C180279_SlaveStatus SlaveStatus
    {
      get { return (C180279_SlaveStatus)GetValue(SlaveStatusProperty); }
      set { SetValue(SlaveStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for SlaveStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty SlaveStatusProperty =
        DependencyProperty.Register("SlaveStatus", typeof(C180279_SlaveStatus), typeof(CtrlSlave), new PropertyMetadata(null));


    #endregion


    private Dictionary<int, Grid> _grdDic = new Dictionary<int, Grid>();

    #region Costruttore
    public CtrlSlave()
    {
      InitializeComponent();
      
    }


    #endregion
  }
}
