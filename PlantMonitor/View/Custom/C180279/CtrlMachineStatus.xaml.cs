﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Configuration;
using Model.Common;
using Model.Custom.C180279;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;

namespace View.Custom.C180279
{
  /// <summary>
  /// Interaction logic for CtrlMonitorManager.xaml
  /// </summary>
  public partial class CtrlMachineStatus : CtrlBaseC180279
  {
    #region TIMER
    DispatcherTimer _dt_DateTime = null;
    DispatcherTimer _dt_MasterAndSlave = null;
    DispatcherTimer _dt_Status = null;
    DispatcherTimer _dt_ProductionInfo = null;
    Dictionary<int, C180279_FloorStatus> _floorStatus = new Dictionary<int, C180279_FloorStatus>();

    List<C180279_MasterStatus> _mastersStatus = new List<C180279_MasterStatus>();
    List<C180279_SlaveStatus> _slavesStatus = new List<C180279_SlaveStatus>();

    #endregion

    #region CONSTRUCTOR
    public CtrlMachineStatus()
    {

      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m1" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m2" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m3" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m4" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m5" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m6" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m7" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m8" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m9" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m10" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m11" });
      _mastersStatus.Add(new C180279_MasterStatus() { Code = "m12" });

      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s1" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s2" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s3" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s4" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s5" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s6" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s7" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s8" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s9" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s10" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s11" });
      _slavesStatus.Add(new C180279_SlaveStatus() { Code = "s12" });

      InitializeComponent();

      #region Inizializzazione Timers

      //get parameters...
      var refreshSettings = ConfigurationManager.Parameters.Setting.Where(x => x.Title == "Refresh").FirstOrDefault();
      int refreshTimeS = refreshSettings.Settings.Where(x => x.Name == "RefreshTime").FirstOrDefault().Value.ConvertToInt();

      _dt_MasterAndSlave = new DispatcherTimer(DispatcherPriority.Send);
      _dt_MasterAndSlave.Interval = TimeSpan.FromSeconds(refreshTimeS);
      _dt_MasterAndSlave.Tick += _dt_MasterAndSlave_Tick;
      _dt_MasterAndSlave.Start();

      _dt_DateTime = new DispatcherTimer(DispatcherPriority.Send);
      _dt_DateTime.Interval = TimeSpan.FromSeconds(1);
      _dt_DateTime.Tick += _dt_DateTime_Tick;
      _dt_DateTime.Start();

      //_dt_Status = new DispatcherTimer(DispatcherPriority.Send);
      //_dt_Status.Interval = TimeSpan.FromSeconds(Properties.Settings.Default.RefreshStatusS);
      //_dt_Status.Tick += _dt_Status_Tick;
      //_dt_Status.Start();

      //_dt_ProductionInfo = new DispatcherTimer(DispatcherPriority.Send);
      //_dt_ProductionInfo.Interval = TimeSpan.FromSeconds(Properties.Settings.Default.RefreshProductionInfoS);
      //_dt_ProductionInfo.Tick += _dt_ProductionInfo_Tick;
      //_dt_ProductionInfo.Start();

      #endregion

      #region Inizializzazione Floor Status
      for (int i = 1; i <= 12; i++)
      {
        C180279_FloorStatus floor = new C180279_FloorStatus();
        floor.FloorNumber = i;
        _floorStatus.Add(i, floor);
      }

      ctrlFloorStatus1.FloorStatus = _floorStatus[1];
      ctrlFloorStatus2.FloorStatus = _floorStatus[2];
      ctrlFloorStatus3.FloorStatus = _floorStatus[3];
      ctrlFloorStatus4.FloorStatus = _floorStatus[4];
      ctrlFloorStatus5.FloorStatus = _floorStatus[5];
      ctrlFloorStatus6.FloorStatus = _floorStatus[6];
      ctrlFloorStatus7.FloorStatus = _floorStatus[7];
      ctrlFloorStatus8.FloorStatus = _floorStatus[8];
      ctrlFloorStatus9.FloorStatus = _floorStatus[9];
      ctrlFloorStatus10.FloorStatus = _floorStatus[10];
      ctrlFloorStatus11.FloorStatus = _floorStatus[11];
      ctrlFloorStatus12.FloorStatus = _floorStatus[12];

      ctrlFloorStatus1.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m1"), _slavesStatus.FirstOrDefault(m => m.Code == "s1"));
      ctrlFloorStatus2.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m2"), _slavesStatus.FirstOrDefault(m => m.Code == "s2"));
      ctrlFloorStatus3.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m3"), _slavesStatus.FirstOrDefault(m => m.Code == "s3"));
      ctrlFloorStatus4.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m4"), _slavesStatus.FirstOrDefault(m => m.Code == "s4"));
      ctrlFloorStatus5.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m5"), _slavesStatus.FirstOrDefault(m => m.Code == "s5"));
      ctrlFloorStatus6.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m6"), _slavesStatus.FirstOrDefault(m => m.Code == "s6"));
      ctrlFloorStatus7.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m7"), _slavesStatus.FirstOrDefault(m => m.Code == "s7"));
      ctrlFloorStatus8.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m8"), _slavesStatus.FirstOrDefault(m => m.Code == "s8"));
      ctrlFloorStatus9.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m9"), _slavesStatus.FirstOrDefault(m => m.Code == "s9"));
      ctrlFloorStatus10.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m10"), _slavesStatus.FirstOrDefault(m => m.Code == "s10"));
      ctrlFloorStatus11.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m11"), _slavesStatus.FirstOrDefault(m => m.Code == "s11"));
      ctrlFloorStatus12.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m12"), _slavesStatus.FirstOrDefault(m => m.Code == "s12"));
      #endregion

      #region visibilità floor status
      if (!Context.Instance.Position.IsNullOrEmpty())
      {
        switch (Context.Instance.Position)
        {
          case "MONITOR_WH1_NORTH":
            columnSouth.Width = new System.Windows.GridLength(0);
            break;

          case "MONITOR_WH2_SOUTH":
            columnNorth.Width = new System.Windows.GridLength(0);
            break;
        }
      }

      #endregion

    }




    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkFloor1.Text = Localization.Localize.LocalizeDefaultString("FLOOR");
      txtBlkFloor2.Text = Localization.Localize.LocalizeDefaultString("FLOOR");
      txtBlkFloor3.Text = Localization.Localize.LocalizeDefaultString("FLOOR");
      txtBlkFloor4.Text = Localization.Localize.LocalizeDefaultString("FLOOR");
      txtBlkFloor5.Text = Localization.Localize.LocalizeDefaultString("FLOOR");
      txtBlkFloor6.Text = Localization.Localize.LocalizeDefaultString("FLOOR");
    }

    #endregion TRADUZIONI

    #region EVENTI TIMERS
    private void _dt_MasterAndSlave_Tick(object sender, EventArgs e)
    {
      Refresh_MasterAndSlave();
    }
    //private void _dt_ProductionInfo_Tick(object sender, EventArgs e)
    //{
    //  Refresh_ProductionInfo();
    //}
    private void _dt_Status_Tick(object sender, EventArgs e)
    {
      Refresh_Status();
    }
    private void _dt_DateTime_Tick(object sender, EventArgs e)
    {
      Refresh_DateTime();
    } 
    #endregion

    #region COMANDI
    //private void Cmd_RM_Monitor_WarehouseStatus()
    //{
    //  CommandManagerC180279.RM_Monitor_WarehouseStatus(this);
    //}

    //private void RM_Monitor_WarehouseStatus(IList<string> commandMethodParameters, IModel model)
    //{
    //  var dwr = model as DataWrapperResult;
    //  if (dwr != null)
    //  {
    //    Context.Instance.ServerStatus.IsConnected = true;

    //    var fsL = dwr.Data as List<C180279_FloorStatus>;
    //    foreach (var floor in fsL)
    //    {
    //      if(_floorStatus.ContainsKey(floor.FloorNumber))
    //      {
    //        _floorStatus[floor.FloorNumber].Update(floor);
    //      }
    //    }

    //  }
    //  else
    //  {
    //    Context.Instance.ServerStatus.IsConnected = false;
    //  }
    //}


    //private void Cmd_C180279_Monitor_ProductionInfo()
    //{
    //  CommandManagerC180279.C180279_Monitor_ProductionInfo(this);
    //}

    //private void C180279_Monitor_ProductionInfo(IList<string> commandMethodParameters, IModel model)
    //{
    //  var dwr = model as DataWrapperResult;
    //  if (dwr != null)
    //  {
    //    Context.Instance.ServerStatus.IsConnected = true;

    //    var pid = dwr.Data as C180279_ProdutionInfoDays;
    //    Context.ProductionInfoYesterday.NumPalletIn = pid.Yesterday.NumPalletIn;
    //    Context.ProductionInfoYesterday.NumPalletOut = pid.Yesterday.NumPalletOut;

    //    Context.ProductionInfoToday.NumPalletIn = pid.Today.NumPalletIn;
    //    Context.ProductionInfoToday.NumPalletOut = pid.Today.NumPalletOut;
    //  }

    //  else
    //  {
    //    Context.Instance.ServerStatus.IsConnected = false;
    //  }
    //}


    private void Cmd_RM_Monitor_MasterStatus()
    {
      CommandManagerC180279.RM_Monitor_MasterStatus(this);
    }
    private void RM_Monitor_MasterStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr != null)
      {
        Context.Instance.ServerStatus.IsConnected = true;
        var mL = dwr.Data as List<C180279_MasterStatus>;
        if(mL!=null)
        {
          foreach (var newMaster in mL)
          {
            var master = _mastersStatus.FirstOrDefault(m => m.Code.ToLower() == newMaster.Code.ToLower());
            master?.Update(newMaster);

          }
        }
      }

      else
      {
        Context.Instance.ServerStatus.IsConnected = false;
      }
    }


    private void Cmd_RM_Monitor_SatelliteStatus()
    {
      CommandManagerC180279.RM_Monitor_SatelliteStatus(this);
    }
    private void RM_Monitor_SatelliteStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr != null)
      {
        Context.Instance.ServerStatus.IsConnected = true;
        var sL = dwr.Data as List<C180279_SlaveStatus>;
        if (sL != null)
        {
          foreach (var newSlave in sL)
          {
            var slave = _slavesStatus.FirstOrDefault(s => s.Code.ToLower() == newSlave.Code.ToLower());
            slave?.Update(newSlave);
          }
        }
      }

      else
      {
        Context.Instance.ServerStatus.IsConnected = false;
      }
    }



    #endregion





    #region METODI REFRESH

    private void Refresh_MasterAndSlave()
    {
      Cmd_RM_Monitor_MasterStatus();
      Cmd_RM_Monitor_SatelliteStatus();
    }

    private void Refresh_DateTime()
    {
      Context.CurrentDate.CurrentDateTime = DateTime.Now;
    }

    private void Refresh_Status()
    {
      //Cmd_RM_Monitor_WarehouseStatus();
    }

    //private void Refresh_ProductionInfo()
    //{
    //  Cmd_C180279_Monitor_ProductionInfo();
    //}

    #endregion
    #region Eventi
    private void CtrlBaseC80279_Loaded(object sender, RoutedEventArgs e)
    {
      Refresh_Status();
      Refresh_MasterAndSlave();
      Translate();
    }
    #endregion

   

  }

}


