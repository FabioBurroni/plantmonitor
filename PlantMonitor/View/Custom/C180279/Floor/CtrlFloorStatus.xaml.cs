﻿using System.Windows;
using System.Windows.Controls;
using Model.Custom.C180279;

namespace View.Custom.C180279.Floor
{
  /// <summary>
  /// Interaction logic for CtrlFloorStatus.xaml
  /// </summary>
  public partial class CtrlFloorStatus : UserControl
  {

    #region DP - FloorStatus
    public C180279_FloorStatus FloorStatus
    {
      get { return (C180279_FloorStatus)GetValue(FloorStatusProperty); }
      set { SetValue(FloorStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for FloorStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty FloorStatusProperty =
        DependencyProperty.Register("FloorStatus", typeof(C180279_FloorStatus), typeof(CtrlFloorStatus), new PropertyMetadata(null));

    #endregion



    #region Costruttore
    public CtrlFloorStatus()
    {
      InitializeComponent();
    } 
    #endregion


    public void SetMasterAndSlave(C180279_MasterStatus master, C180279_SlaveStatus slave)
    {
      ctrlMaster.MasterStatus = master;
      ctrlSlave.SlaveStatus = slave;
    }



  }
}
