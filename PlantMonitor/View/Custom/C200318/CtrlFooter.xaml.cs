﻿using System.Windows;
using System.Windows.Controls;

namespace View.Custom.C200318
{
  /// <summary>
  /// Interaction logic for CtrlFooter.xaml
  /// </summary>
  public partial class CtrlFooter : CtrlBaseC200318
  {

    public string AppVersion
    {
      get { return (string)GetValue(AppVersionProperty); }
      set { SetValue(AppVersionProperty, value); }
    }

    // Using a DependencyProperty as the backing store for AppVersion.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty AppVersionProperty =
      DependencyProperty.Register("AppVersion", typeof(string), typeof(CtrlFooter), new PropertyMetadata(""));

    public CtrlFooter()
    {
      InitializeComponent();
    }

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkLocalAddress.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkLocalAddress.Tag);
      txtBlkServerAddress.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkServerAddress.Tag);
    }

    #endregion TRADUZIONI

    private void CtrlBaseC200318_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Translate();
    }
  }
}
