﻿using System.Windows.Controls;

namespace View.Custom.C200318
{
  /// <summary>
  /// Interaction logic for CtrlHeader.xaml
  /// </summary>
  public partial class CtrlHeader : CtrlBaseC200318
  {
    public CtrlHeader()
    {
      InitializeComponent();
    }

    #region TRADUZIONI

    protected override void Translate()
    {
      
    }

    #endregion TRADUZIONI

    private void CtrlBaseC200318_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
      Translate();
    }
  }
}
