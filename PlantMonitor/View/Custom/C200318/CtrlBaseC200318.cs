﻿using Configuration;
using Model.Custom.C200318;
using View.Common;
using Utilities.Extensions;
using XmlCommunicationManager.XmlClient.Event;

namespace View.Custom.C200318
{
  public class CtrlBaseC200318 : CtrlBase
  {
    public CtrlBaseC200318()
    {
      // Check for design mode. 
      if (DesignTimeHelper.IsInDesignMode)
        return;

      InitCommandManager();

    }

    #region Private Properties

    protected C200318_CommandManager CommandManagerC200318 { get; private set; }

    #endregion

    #region Init CommandManager

    private void InitCommandManager()
    {
      CommandManagerC200318 = new C200318_CommandManager(Context.XmlClient, ConfigurationManager.Parameters.CommandPreambleSetting.GetPreamble("custom"));
    }

    #endregion

    #region Override

    protected override void XmlClientOnException(object sender, ExceptionEventArgs e)
    {
      if (ManageXmlClientException(CommandManagerC200318, e))
        return;

      base.XmlClientOnException(sender, e);
    }

    protected override void XmlClientOnRetrieveReplyFromServer(object sender, MsgEventArgs e)
    {
      if (ManageXmlClientReply(CommandManagerC200318, e))
        return;

      base.XmlClientOnRetrieveReplyFromServer(sender, e);
    }

    #endregion

  }
}
