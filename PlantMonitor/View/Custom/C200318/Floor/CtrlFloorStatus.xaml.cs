﻿using System.Windows;
using System.Windows.Controls;
using Model.Custom.C200318;

namespace View.Custom.C200318.Floor
{
  /// <summary>
  /// Interaction logic for CtrlFloorStatus.xaml
  /// </summary>
  public partial class CtrlFloorStatus : UserControl
  {

    #region DP - FloorStatus
    public C200318_FloorStatus FloorStatus
    {
      get { return (C200318_FloorStatus)GetValue(FloorStatusProperty); }
      set { SetValue(FloorStatusProperty, value); }
    }

    // Using a DependencyProperty as the backing store for FloorStatus.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty FloorStatusProperty =
        DependencyProperty.Register("FloorStatus", typeof(C200318_FloorStatus), typeof(CtrlFloorStatus), new PropertyMetadata(null));

    #endregion



    #region Costruttore
    public CtrlFloorStatus()
    {
      InitializeComponent();
    } 
    #endregion


    public void SetMasterAndSlave(C200318_MasterStatus master, C200318_SlaveStatus slave)
    {
      ctrlMaster.MasterStatus = master;
      ctrlSlave.SlaveStatus = slave;
    }



  }
}
