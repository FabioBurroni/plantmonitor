﻿using Model.Custom.C200318;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace View.Custom.C200318.Converter

{
  public class SatAlarmServiceToColorConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C200318_AlarmServiceStatusEnum)
      {
        var status = (C200318_AlarmServiceStatusEnum)value;

        switch (status)
        {
          case C200318_AlarmServiceStatusEnum.UNDEFINED:
            return Brushes.Gray;
          case C200318_AlarmServiceStatusEnum.OK:
            return Brushes.Green;
          case C200318_AlarmServiceStatusEnum.ALARM:
            return Brushes.Red;
          case C200318_AlarmServiceStatusEnum.SERVICE:
           // return Brushes.Gold; Non si mostrano i service...
            return Brushes.Green;
          default:
            return Brushes.Gray;
        }
      }
        return Brushes.Gray;

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
