﻿using Model.Custom.C200318_2;
using System;
using System.Globalization;
using System.Windows.Data;

namespace View.Custom.C200318_2.Converter

{
  public class SatStatusToStringConverter : IValueConverter
  {

    public bool IsForeground { get; set; } = true;

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if(value is C200318_SatelliteStatusEnum)
      {
        var status = (C200318_SatelliteStatusEnum)value;

        switch (status)
        {
          case C200318_SatelliteStatusEnum.PLAY:
            return Localization.Localize.LocalizeDefaultString("PLAY");
          case C200318_SatelliteStatusEnum.STOP:
            return Localization.Localize.LocalizeDefaultString("STOP");
          case C200318_SatelliteStatusEnum.ERROR:
            return Localization.Localize.LocalizeDefaultString("ERROR");
          case C200318_SatelliteStatusEnum.RESTORE:
            return Localization.Localize.LocalizeDefaultString("RESTORE");
          case C200318_SatelliteStatusEnum.RESET:
            return Localization.Localize.LocalizeDefaultString("RESET");
          case C200318_SatelliteStatusEnum.CHARGING:
            return Localization.Localize.LocalizeDefaultString("CHARGING");
          case C200318_SatelliteStatusEnum.PARKING:            
            return Localization.Localize.LocalizeDefaultString("PARKING");
          case C200318_SatelliteStatusEnum.MANUAL:
            return Localization.Localize.LocalizeDefaultString("MANUAL");
          default:
            return "";
        }
      }
      return "";

    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
