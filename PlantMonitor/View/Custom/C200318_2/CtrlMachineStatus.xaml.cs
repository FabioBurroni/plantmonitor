﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Configuration;
using Model.Common;
using Model.Custom.C200318_2;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;

namespace View.Custom.C200318_2
{
  /// <summary>
  /// Interaction logic for CtrlMonitorManager.xaml
  /// </summary>
  public partial class CtrlMachineStatus : CtrlBaseC200318
  {

    #region TIMER
    DispatcherTimer _dt_DateTime = null;
    DispatcherTimer _dt_MasterAndSlave = null;
    DispatcherTimer _dt_Status = null;
    DispatcherTimer _dt_ProductionInfo = null;
    Dictionary<int, C200318_FloorStatus> _floorStatus = new Dictionary<int, C200318_FloorStatus>();

    List<C200318_MasterStatus> _mastersStatus = new List<C200318_MasterStatus>();
    List<C200318_SlaveStatus> _slavesStatus = new List<C200318_SlaveStatus>();

    #endregion

    #region CONSTRUCTOR
    public CtrlMachineStatus()
    {

      _mastersStatus.Add(new C200318_MasterStatus() { Code = "m1" });

      _slavesStatus.Add(new C200318_SlaveStatus() { Code = "s1" });
      _slavesStatus.Add(new C200318_SlaveStatus() { Code = "s2" });

      InitializeComponent();

      #region Inizializzazione Timers

      //get parameters...
      var refreshSettings = ConfigurationManager.Parameters.Setting.Where(x => x.Title == "Refresh").FirstOrDefault();
      int refreshTimeS = refreshSettings.Settings.Where(x => x.Name == "RefreshTime").FirstOrDefault().Value.ConvertToInt();

      _dt_MasterAndSlave = new DispatcherTimer(DispatcherPriority.Send);
      _dt_MasterAndSlave.Interval = TimeSpan.FromSeconds(refreshTimeS);
      _dt_MasterAndSlave.Tick += _dt_MasterAndSlave_Tick;
      _dt_MasterAndSlave.Start();

      _dt_DateTime = new DispatcherTimer(DispatcherPriority.Send);
      _dt_DateTime.Interval = TimeSpan.FromSeconds(1);
      _dt_DateTime.Tick += _dt_DateTime_Tick;
      _dt_DateTime.Start();

      #endregion

      #region Inizializzazione Floor Status
      for (int i = 1; i <= 12; i++)
      {
        C200318_FloorStatus floor = new C200318_FloorStatus();
        floor.FloorNumber = i;
        _floorStatus.Add(i, floor);
      }

      ctrlFloorStatus1.FloorStatus = _floorStatus[1];

      ctrlFloorStatus1.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m1"), _slavesStatus.FirstOrDefault(m => m.Code == "s1"));
      ctrlFloorStatus2.SetMasterAndSlave(_mastersStatus.FirstOrDefault(m => m.Code == "m1"), _slavesStatus.FirstOrDefault(m => m.Code == "s2"));
      #endregion

      #region visibilità floor status
      if (!Context.Instance.Position.IsNullOrEmpty())
      {
        switch (Context.Instance.Position)
        {
          case "MONITOR_WH1_NORTH":
            columnSouth.Width = new System.Windows.GridLength(0);
            break;

          case "MONITOR_WH2_SOUTH":
            columnNorth.Width = new System.Windows.GridLength(0);
            break;
        }
      }

      #endregion

    }




    #endregion

    #region TRADUZIONI

    protected override void Translate()
    {
      txtBlkFloor1.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkFloor1.Tag);
      txtBlkFloor2.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkFloor2.Tag);
      txtBlkFloor3.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkFloor3.Tag);
      txtBlkFloor4.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkFloor4.Tag);
      txtBlkFloor5.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkFloor5.Tag);
      txtBlkFloor6.Text = Localization.Localize.LocalizeDefaultString((string)txtBlkFloor6.Tag);
    }

    #endregion TRADUZIONI

    #region EVENTI TIMERS
    private void _dt_MasterAndSlave_Tick(object sender, EventArgs e)
    {
      Refresh_MasterAndSlave();
    }
    //private void _dt_ProductionInfo_Tick(object sender, EventArgs e)
    //{
    //  Refresh_ProductionInfo();
    //}
    private void _dt_Status_Tick(object sender, EventArgs e)
    {
      Refresh_Status();
    }
    private void _dt_DateTime_Tick(object sender, EventArgs e)
    {
      Refresh_DateTime();
    } 
    #endregion

    #region COMANDI
    
    private void Cmd_RM_Monitor_MasterStatus()
    {
      CommandManagerC200318.RM_Monitor_MasterStatus(this);
    }
    private void RM_Monitor_MasterStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr != null)
      {
        Context.Instance.ServerStatus.IsConnected = true;
        var mL = dwr.Data as List<C200318_MasterStatus>;
        if(mL!=null)
        {
          foreach (var newMaster in mL)
          {
            var master = _mastersStatus.FirstOrDefault(m => m.Code.ToLower() == newMaster.Code.ToLower());
            master?.Update(newMaster);

          }
        }
      }

      else
      {
        Context.Instance.ServerStatus.IsConnected = false;
      }
    }


    private void Cmd_RM_Monitor_SatelliteStatus()
    {
      CommandManagerC200318.RM_Monitor_SatelliteStatus(this);
    }
    private void RM_Monitor_SatelliteStatus(IList<string> commandMethodParameters, IModel model)
    {
      var dwr = model as DataWrapperResult;
      if (dwr != null)
      {
        Context.Instance.ServerStatus.IsConnected = true;
        var sL = dwr.Data as List<C200318_SlaveStatus>;
        if (sL != null)
        {
          foreach (var newSlave in sL)
          {
            var slave = _slavesStatus.FirstOrDefault(s => s.Code.ToLower() == newSlave.Code.ToLower());
            slave?.Update(newSlave);
          }
        }
      }

      else
      {
        Context.Instance.ServerStatus.IsConnected = false;
      }
    }

    #endregion

    #region METODI REFRESH

    private void Refresh_MasterAndSlave()
    {
      Cmd_RM_Monitor_MasterStatus();
      Cmd_RM_Monitor_SatelliteStatus();
    }

    private void Refresh_DateTime()
    {
      Context.CurrentDate.CurrentDateTime = DateTime.Now;
    }

    private void Refresh_Status()
    {
      //Cmd_RM_Monitor_WarehouseStatus();
    }

    #endregion
   
    #region Eventi
    private void CtrlBaseC80279_Loaded(object sender, RoutedEventArgs e)
    {
      Refresh_Status();
      Refresh_MasterAndSlave();
      Translate();

    }
    #endregion
    
  }

}


