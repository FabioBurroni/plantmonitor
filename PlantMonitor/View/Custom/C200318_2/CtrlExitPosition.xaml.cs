﻿using Configuration;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Utilities.Extensions;
using Model.Common;
using Model.Custom.C200318_2;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using System.Collections.Generic;
using XmlCommunicationManager.XmlServer;
using XmlCommunicationManager.XmlClient;
using Utilities.Threading;
using XmlCommunicationManager.XmlServer.Event;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.XmlClient.Event;

namespace View.Custom.C200318_2
{
  /// <summary>
  /// Interaction logic for CtrlExitPosition.xaml
  /// </summary>
  public partial class CtrlExitPosition : CtrlBaseC200318
  {
    #region TIMER
    DispatcherTimer _dt_DateTime = null;
    DispatcherTimer _dt_PalletInPos = null;
    DispatcherTimer _dt_UpdateConf = null;
    #endregion

    bool isAutoUpdate = false;

    #region PUBLIC PROPS
    private C200318_Pallet _pallet = new C200318_Pallet();

    public C200318_Pallet Pallet
    {
      get { return _pallet; }
      set 
      {
        _pallet = value;
        NotifyPropertyChanged();
      }
    }
    #endregion

    #region CONSTRUCTOR
    public CtrlExitPosition()
    {
      InitializeComponent();

      SetData();
      
      Context.Instance.ServerStatus.IsConnected = false;

      #region Inizializzazione Timers

      //get parameters...
      var refreshSettings = ConfigurationManager.Parameters.Setting.Where(x => x.Title == "Refresh").FirstOrDefault();
      int refreshTimeS = refreshSettings.Settings.Where(x => x.Name == "RefreshTime").FirstOrDefault().Value.ConvertToInt();

      _dt_DateTime = new DispatcherTimer(DispatcherPriority.Send);
      _dt_DateTime.Interval = TimeSpan.FromSeconds(1);
      _dt_DateTime.Tick += _dt_DateTime_Tick;
      _dt_DateTime.Start();

      _dt_PalletInPos = new DispatcherTimer(DispatcherPriority.Send);
      _dt_PalletInPos.Interval = TimeSpan.FromSeconds(refreshTimeS);
      _dt_PalletInPos.Tick += _dt_PalletInPos_Tick;
      _dt_PalletInPos.Start();

      _dt_UpdateConf = new DispatcherTimer(DispatcherPriority.Send);
      _dt_UpdateConf.Interval = TimeSpan.FromMinutes(15);
      _dt_UpdateConf.Tick += _dt_UpdateConf_Tick;
      _dt_UpdateConf.Start();
      #endregion
    }


    #endregion


    #region 29/04/2022 Added for Authentication an update over Notifications

    private IXmlServer _xmlServer;

    public void SetData()
    {

      //...get instance of xml server
      _xmlServer = Context.XmlServer;
      _xmlServer.OnRequest += XmlServerOnRequest;

    }

    private void XmlServerOnRequest(object sender, XmlOnRequestEventArgs e)
    {
      var xc = (XmlCommand)e.request.request;

      string commandName = xc.commandMethodName;

      //...preparazione risposta
      XmlCommandResponse xmlCmdRes = new XmlCommandResponse(xc);

      #region Notification

      if (commandName.EqualsConsiderCase("Notify_Pallet_OnPosition_Changed"))
      {
        Cmd_RM_Pallet_On_Exit_Pos();
      }

      #endregion

      //...send back and empty answer 
      XmlResponse xmlr = new XmlResponse(e.request);
      xmlr.setResponse(xmlCmdRes);
      _xmlServer?.SendResponse(xmlr);
    }

    private void XmlServer_Close()
    {
      if(_xmlServer!=null)
        _xmlServer.OnRequest -= XmlServerOnRequest;
    }

    public override void Closing()
    {
      XmlServer_Close();

      if (_dt_DateTime != null)
      {

        _dt_DateTime.Stop();
        _dt_DateTime.Tick -= _dt_DateTime_Tick;
        _dt_DateTime = null;

      }
      if (_dt_PalletInPos!=null)
       {
        _dt_PalletInPos.Stop();
        _dt_PalletInPos.Tick -= _dt_PalletInPos_Tick;
        _dt_PalletInPos = null;
       }

       if(_dt_UpdateConf!=null)
       {
        _dt_UpdateConf.Stop();
        _dt_UpdateConf.Tick -= _dt_UpdateConf_Tick;
        _dt_UpdateConf = null;
       }

      base.Closing();
    }


    #endregion



    #region TRANSLATIONS

    protected override void Translate()
    {
      tbDestinationMachine.Text = Localization.Localize.LocalizeDefaultString((string)tbDestinationMachine.Tag);
      tbOrderCode.Text = Localization.Localize.LocalizeDefaultString((string)tbOrderCode.Tag);
      tbLotCode.Text = Localization.Localize.LocalizeDefaultString((string)tbLotCode.Tag);
      tbQuantity.Text = Localization.Localize.LocalizeDefaultString((string)tbQuantity.Tag);
      tbPalletCode.Text = Localization.Localize.LocalizeDefaultString((string)tbPalletCode.Tag);
      tbArticleCode.Text = Localization.Localize.LocalizeDefaultString((string)tbArticleCode.Tag);
      tbQtyToTake.Text = Localization.Localize.LocalizeDefaultString((string)tbQtyToTake.Tag);
      tbLotCodeNO.Text = Localization.Localize.LocalizeDefaultString((string)tbLotCodeNO.Tag);
      tbArticleCodeNO.Text = Localization.Localize.LocalizeDefaultString((string)tbArticleCodeNO.Tag);
      tbPalletCodeNO.Text = Localization.Localize.LocalizeDefaultString((string)tbPalletCodeNO.Tag);
      tbQuantityNO.Text = Localization.Localize.LocalizeDefaultString((string)tbQuantityNO.Tag);
      tbPositionEmpty.Text = Localization.Localize.LocalizeDefaultString((string)tbPositionEmpty.Tag);
      tbServicePalletCode.Text = Localization.Localize.LocalizeDefaultString((string)tbServicePalletCode.Tag);
      tbServicePallet.Text = Localization.Localize.LocalizeDefaultString((string)tbServicePallet.Tag);
    }

    #endregion TRANSLATIONS

    #region TIMERS EVENTS
    private void _dt_DateTime_Tick(object sender, EventArgs e)
    {
      Refresh_DateTime();
    }

    private void _dt_PalletInPos_Tick(object sender, EventArgs e)
    {
      //if update on timer, not by notification
      if(isAutoUpdate)
        Cmd_RM_Pallet_On_Exit_Pos();
    }

    private void _dt_UpdateConf_Tick(object sender, EventArgs e)
    {
      Cmd_RM_PlantMonitorAutoUpdate();
    }
    #endregion

    #region COMANDS

    private void Cmd_RM_Pallet_On_Exit_Pos()
    {
      var pos = Context.Position;
      CommandManagerC200318.RM_Pallet_On_Exit_Pos(this, pos);
    }
    private void RM_Pallet_On_Exit_Pos(IList<string> commandMethodParameters, IModel model)
    {
      Context.Instance.ServerStatus.IsConnected = true;
      
      var dwr = model as DataWrapperResult;
      if (dwr != null)
      {
        var pal = dwr.Data as C200318_Pallet;
        if(pal != null)
        {
          Pallet.ArticleCode = pal.ArticleCode;
          Pallet.DestinationMachine = pal.DestinationMachine;
          Pallet.IsService = pal.IsService;
          Pallet.KarinaQtyDelivered = pal.KarinaQtyDelivered;
          Pallet.KarinaQtyToTake = pal.KarinaQtyToTake;
          Pallet.LotCode = pal.LotCode;
          Pallet.OrderCmpRequestedQty = pal.OrderCmpRequestedQty;
          Pallet.OrderCode = pal.OrderCode;
          Pallet.PalletCode = pal.PalletCode;
          Pallet.ProductQty = pal.ProductQty;
          Pallet.Seq = pal.Seq;

          if(pal.IsService)
            tbctrlPosition.SelectedIndex = 2;
          else if(!pal.IsService && pal.OrderCode.IsNullOrEmpty())
            tbctrlPosition.SelectedIndex = 1;
          else if(!pal.IsService && pal.OrderCode != "")
            tbctrlPosition.SelectedIndex = 0;
        }
        else
          tbctrlPosition.SelectedIndex = 3;
      }
      else
      {
        tbctrlPosition.SelectedIndex = 3;
      }
    }

    private void Cmd_RM_PlantMonitorAutoUpdate()
    {
      CommandManagerC200318.RM_PlantMonitorAutoUpdate(this);
    }
    private void RM_PlantMonitorAutoUpdate(IList<string> commandMethodParameters, IModel model)
    {
      Context.Instance.ServerStatus.IsConnected = true;

      var dwr = model as DataWrapperResult;
      if(dwr != null)
      {
        var str = dwr.Data as string;
        if (str == true.ToString())
          isAutoUpdate = true;
        else
          isAutoUpdate = false;
      }
    }

    #endregion

    #region EVENTS
    private void CtrlBaseC80279_Loaded(object sender, RoutedEventArgs e)
    {
      Translate();
      Cmd_RM_Pallet_On_Exit_Pos();
      Cmd_RM_PlantMonitorAutoUpdate();
    }
    #endregion

    #region REFRESH Methods

    private void Refresh_DateTime()
    {
      Context.CurrentDate.CurrentDateTime = DateTime.Now;
    }

    #endregion

  }
}
