﻿using System.IO;
using System.Xml;
using System.Xml.XPath;
using Utilities.Extensions;

namespace Configuration
{
	public static class ConfigurationWriter
	{		

		public static bool SetLayoutValue(string sectionTitle, string settingName, string value)
		{
			bool ret = false;

			if (!File.Exists(ConfigurationManager.LayoutFile))
				throw new FileNotFoundException("Configuration file non found", ConfigurationManager.LayoutFile);

			XmlDocument _xDoc = new XmlDocument();
			_xDoc.Load(ConfigurationManager.LayoutFile);
			XPathNavigator _xNav = _xDoc.CreateNavigator();

			XPathNodeIterator ite = _xNav.Select("/configuration");
			if (ite.MoveNext())
			{
				XPathNodeIterator itera = ite.Current.SelectChildren(XPathNodeType.All);
				while (itera.MoveNext())
				{
					if (itera.Current.Name.EqualsIgnoreCase("layout"))
					{
						XPathNodeIterator itIn = itera.Current.SelectChildren(XPathNodeType.All);

						while (itIn.MoveNext())
						{
							if (itIn.Current.Name.EqualsIgnoreCase("LayoutSection"))
							{
								if (sectionTitle == itIn.Current.GetAttribute("title", ""))
								{
									XPathNodeIterator iteraS = itIn.Current.SelectChildren(XPathNodeType.All);
									while (iteraS.MoveNext())
									{
										if (iteraS.Current.Name.EqualsIgnoreCase("setting"))
										{
											if (settingName == iteraS.Current.GetAttribute("name", ""))
											{
												iteraS.Current.SetValue(value);
												ret = true;
												_xDoc.Save(ConfigurationManager.LayoutFile);
												ConfigurationReader reader = new ConfigurationReader(ConfigurationManager.Parameters);
												reader.LoadConfiguration(ConfigurationManager.LayoutFile);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			
			return ret;
		}
	
		

	}
}