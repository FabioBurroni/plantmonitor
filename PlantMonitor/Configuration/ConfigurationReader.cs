﻿using System;
using System.IO;
using System.Xml.XPath;
using Utilities.Extensions;

namespace Configuration
{
  public class ConfigurationReader : BaseConfigurationReader
  {
    #region Fields

    private XPathDocument _xDoc;
    private XPathNavigator _xNav;

    private readonly ConfigurationParameters _parameters;

    #endregion

    #region Constructor

    public ConfigurationReader(ConfigurationParameters parameters)
      : base(parameters)
    {
      if (parameters == null)
        throw new ArgumentNullException(nameof(parameters));

      _parameters = parameters;
    }

    #endregion

    #region Public Methods

    public override void LoadConfiguration(string filePath)
    {
      if (!File.Exists(filePath))
        throw new FileNotFoundException("Configuration file non found", filePath);

      _xDoc = new XPathDocument(filePath);
      _xNav = _xDoc.CreateNavigator();
      ReadXml();

      base.LoadConfiguration(filePath);
    }

    #endregion

    #region Private Methods

    #region ReadXml

    private void ReadXml()
    {
      XPathNodeIterator ite = _xNav.Select("/configuration");
      if (ite.MoveNext())
      {
        XPathNodeIterator itera = ite.Current.SelectChildren(XPathNodeType.All);
        while (itera.MoveNext())
        {
          //#region COMMESSA C150611

          //#region Database Connection

          //if (itera.Current.Name.EqualsIgnoreCase("db_connection"))
          //{
          //  SetDatabaseConnection(itera.Current);
          //}

          //#endregion

          //#region WebServer

          //else if (itera.Current.Name.EqualsIgnoreCase("webserver"))
          //{
          //  SetWebServer(itera.Current);
          //}

          //#endregion

          //#region cassioli vistual keyboard

          //if (itera.Current.Name.EqualsIgnoreCase("cassioli_virtual_keyboard"))
          //{
          //  SetCassioliVirtualKeyboard(itera.Current);
          //}

          //#endregion

          //#endregion
        }
      }
    }

    #endregion

    //#region cassioli virtual keyboard
    //private void SetCassioliVirtualKeyboard(XPathNavigator ite)
    //{
    //  var enabled = ite.GetAttribute("enabled", "");
    //  _parameters.CassioliVirtualKeyboard = new CassioliVirtualKeyboard(bool.Parse(enabled));
    //}
    //#endregion

    //#region Database Connection

    //private void SetDatabaseConnection(XPathNavigator ite)
    //{
    //  XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);

    //  var connectionName = itera.Current.GetAttribute("name", "");

    //  string catalog = "", dataSource = "", user = "", password = "";

    //  while (itera.MoveNext())
    //  {
    //    if (itera.Current.Name.EqualsIgnoreCase("catalog"))
    //      catalog = itera.Current.Value;
    //    else if (itera.Current.Name.EqualsIgnoreCase("datasource"))
    //      dataSource = itera.Current.Value;
    //    else if (itera.Current.Name.EqualsIgnoreCase("user"))
    //      user = itera.Current.Value;
    //    else if (itera.Current.Name.EqualsIgnoreCase("password"))
    //      password = itera.Current.Value;
    //  }

    //  _parameters.DatabaseConnection = new DatabaseConnection(connectionName, dataSource, catalog, user, password);
    //}

    //#endregion

    //#region WebServer

    //private void SetWebServer(XPathNavigator ite)
    //{
    //  var itera = ite.SelectChildren(XPathNodeType.All);

    //  string url = "";

    //  while (itera.MoveNext())
    //  {
    //    if (itera.Current.Name.EqualsIgnoreCase("url"))
    //      url = itera.Current.Value;
    //  }

    //  _parameters.WebServer = new WebServer(url);
    //}

    //#endregion

    #endregion
  }
}