﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.XPath;
using Configuration.Settings.Global;
using Configuration.Settings.UI;
using Utilities.Extensions;

namespace Configuration
{
  /// <summary>
  /// Classe che si occupa di caricare la configurazione globale a partire dal
  /// file di configurazione xml
  /// </summary>
  public class BaseConfigurationReader
	{
		#region Fields

		private XPathDocument _xDoc;
		private XPathNavigator _xNav;
		private readonly BaseConfigurationParameters _parameters;

		#endregion

		#region Constructor

		public BaseConfigurationReader(BaseConfigurationParameters parameters)
		{
			if (parameters == null)
				throw new ArgumentNullException(nameof(parameters));

			_parameters = parameters;
		}

		#endregion

		#region Public Methods

		public virtual void LoadConfiguration(string filePath)
		{
			if (!File.Exists(filePath))
				throw new FileNotFoundException("Configuration file non found", filePath);

			_xDoc = new XPathDocument(filePath);
			_xNav = _xDoc.CreateNavigator();
			ReadXml();
		}

		#endregion

		#region Private Methods

		#region ReadXml

		private void ReadXml()
		{
			Utilities.Logging.Message("BaseConfigurationReader: caricamento dati....");

			XPathNodeIterator ite = _xNav.Select("/configuration");
			if (ite.MoveNext())
			{
				XPathNodeIterator itera = ite.Current.SelectChildren(XPathNodeType.All);
				while (itera.MoveNext())
				{
					#region DIMENSIONI FINESTRA
					if (itera.Current.Name.EqualsIgnoreCase("window"))
					{
						var mainWindow = new WindowSetting
						{
							Height = itera.Current.GetAttribute("height", "").ConvertTo<int>(),
							Width = itera.Current.GetAttribute("width", "").ConvertTo<int>(),
							Resizable = itera.Current.GetAttribute("resizable", "").ConvertTo<bool>(true),
							CanMinimize = itera.Current.GetAttribute("can_minimize", "").ConvertTo<bool>(true),
							CanMaximize = itera.Current.GetAttribute("can_maximize", "").ConvertTo<bool>(true),
							Maximized = itera.Current.GetAttribute("maximized", "").ConvertTo<bool>(true)
						};

						_parameters.Window = mainWindow;
					}
					#endregion

					#region AUTHENTICATION

					else if (itera.Current.Name.EqualsIgnoreCase("authentication"))
					{
						SetAuthentication(itera.Current);
					}

					#endregion

					#region DATE_FORMAT
					else if (itera.Current.Name.EqualsIgnoreCase("date_format"))
					{
						_parameters.DateFormat = itera.Current.GetAttribute("value", "");
					}
					#endregion

					#region WEIGHT_FORMAT
					else if (itera.Current.Name.EqualsIgnoreCase("weight_format"))
					{
						_parameters.WeightFormat = itera.Current.GetAttribute("value", "");
					}
					#endregion

					#region XML_CLIENT
					else if (itera.Current.Name.EqualsIgnoreCase("xml_client_settings"))
					{
						var ipAddress = itera.Current.GetAttribute("ip_address", "");
						var portNumber = itera.Current.GetAttribute("port_number", "").ConvertTo<int>();
						var timeout = itera.Current.GetAttribute("time_out", "").ConvertTo<int>();
						var version = itera.Current.GetAttribute("version", "").ConvertTo<int>();

						_parameters.XmlClientConnectionSetting = new ConnectionSetting(ipAddress, portNumber, timeout, version);
					}
					#endregion

					#region XML_SERVER
					else if (itera.Current.Name.EqualsIgnoreCase("xml_server_settings"))
					{
						ConnectionSetting xmlServerSetting;

						var enabledString = itera.Current.GetAttribute("enabled", "");
						var enabled = string.IsNullOrEmpty(enabledString) || enabledString.ConvertTo<bool>();

						if (!enabled)
						{
							//se non è da abilitare, nemmeno leggo gli altri dati
							xmlServerSetting = new ConnectionSetting(enabled);
						}
						else
						{
							var ipAddress = itera.Current.GetAttribute("ip_address", "");
							var portNumber = itera.Current.GetAttribute("port_number", "").ConvertTo<int>();
							var timeout = itera.Current.GetAttribute("time_out", "").ConvertTo<int>();
							var version = itera.Current.GetAttribute("version", "").ConvertTo<int>();

							xmlServerSetting = new ConnectionSetting(ipAddress, portNumber, timeout, version, enabled);
						}

						_parameters.XmlServerConnectionSetting = xmlServerSetting;
					}
					#endregion

					#region UPDATE
					else if (itera.Current.Name.EqualsIgnoreCase("update"))
					{
						var feedUrl = itera.Current.GetAttribute("feedUrl", "");
						var checkingIntervalMinutes = itera.Current.GetAttribute("checkingIntervalMinutes", "").ConvertTo<int>(true);
						var remindMeLaterIntervalMinutes = itera.Current.GetAttribute("remindMeLaterIntervalMinutes", "").ConvertTo<int>(true);
						var installWithoutUserConfirm = itera.Current.GetAttribute("installWithoutUserConfirm", "").ConvertTo<bool>(true);

						_parameters.UpdateSetting = new UpdateSetting(feedUrl, checkingIntervalMinutes, remindMeLaterIntervalMinutes, installWithoutUserConfirm);
					}
					#endregion

					#region RUN_APPLICATION_AT_WINDOWS_STARTUP

					else if (itera.Current.Name.EqualsConsiderCase("run_application_at_windows_startup"))
					{
						_parameters.RunApplicationAtWindowsStartup = itera.Current.GetAttribute("enable", "").ConvertTo<bool>(true);
					}

					#endregion
					
					#region COMMAND_PREAMBLE
					else if (itera.Current.Name.EqualsIgnoreCase("command_preambles"))
					{
						SetCommandPreambles(itera.Current);
					}
					#endregion

					#region LOG
					else if (itera.Current.Name.EqualsIgnoreCase("log"))
					{
						SetLogging(itera.Current);
					}
					#endregion

					#region HOMEPAGE
					else if (itera.Current.Name.EqualsIgnoreCase("views"))
					{
						SetViews(itera.Current);
					}
					#endregion

					#region LOCALIZATION
					else if (itera.Current.Name.EqualsIgnoreCase("localization"))
					{
						SetLocalization(itera.Current);
					}
					#endregion

					#region LAYOUT SETTINGS
					else if (itera.Current.Name.EqualsIgnoreCase("layout"))
					{
						SetLayout(itera.Current);
					}
					#endregion

					#region SETTINGS
					else if (itera.Current.Name.EqualsIgnoreCase("setting"))
					{
						SetSettings(itera.Current);
					}
					#endregion
				}
			}
		}

		#endregion

		#region COMMAND PREAMBLE

		private void SetCommandPreambles(XPathNavigator ite)
		{
			var preambles = new CommandPreambleSetting();

			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("command_preamble"))
				{
					var type = itera.Current.GetAttribute("type", "");
					var preamble = itera.Current.Value;

					CommandPreamble commandPreamble = new CommandPreamble(type, preamble);

					preambles.AddPreamble(commandPreamble);
				}
			}

			_parameters.CommandPreambleSetting = preambles;
		}

		#endregion

		#region LOGGING

		private void SetLogging(XPathNavigator ite)
		{
			bool xmlClientLog = true, xmlServerLog = true, generalLog = true;

			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("log_xml_client_commands"))
				{
					xmlClientLog = itera.Current.GetAttribute("value", "").ConvertTo<bool>();
				}
				else if (itera.Current.Name.EqualsIgnoreCase("log_xml_server_commands"))
				{
					xmlServerLog = itera.Current.GetAttribute("value", "").ConvertTo<bool>();
				}
				else if (itera.Current.Name.EqualsIgnoreCase("log_activity"))
				{
					generalLog = itera.Current.GetAttribute("value", "").ConvertTo<bool>();
				}
			}

			_parameters.LogSetting = new LoggingSetting(xmlClientLog, xmlServerLog, generalLog);
		}

		#endregion

		#endregion

		#region Views

		private void SetViews(XPathNavigator ite)
		{
			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);
			Views views = new Views();

			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("view"))
				{
					var name = itera.Current.GetAttribute("name", "");
					var text = itera.Current.GetAttribute("text", "");
					var position = itera.Current.GetAttribute("position", "").ConvertTo<int>();
					var duration = itera.Current.GetAttribute("duration", "").ConvertTo<int>();

					string allowed_position = "";
          try
          {
						allowed_position = itera.Current.GetAttribute("allowed_position", "");
          }
          catch (Exception)
          {
          }

					ViewSection viewSection = new ViewSection(name, text, position, duration, allowed_position);

					SetViewSection(itera.Current, viewSection);

					views.Add(viewSection);
				}
				
			}

			_parameters.Views = views;
		}
	
		private void SetViewSection(XPathNavigator ite, ViewSection viewSection)
		{
			XPathNodeIterator itera = ite.SelectChildren(XPathNodeType.All);

			while (itera.MoveNext())
			{
				if (itera.Current.Name.EqualsIgnoreCase("ctrl"))
				{
					var name = itera.Current.GetAttribute("name", "");
					var container = itera.Current.GetAttribute("container", "");
					var type = itera.Current.GetAttribute("type", "");
					var path = itera.Current.Value;

					viewSection.Add(new ControlSection(name, type, container, path));
					
				}
			}
		}
		
		#endregion

		#region LOCALIZATION/LANGUAGE

		private void SetLocalization(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.All);

			var localization = new Localization();

			while (itIn.MoveNext())
			{
				XPathNodeIterator itera = itIn.Current.SelectChildren(XPathNodeType.All);

				if (itera.Current.Name.EqualsIgnoreCase("language"))
				{
					var name = itera.Current.Value;
					var isDefault = itera.Current.GetAttribute("default", "").ConvertTo<bool>(true);
					var image = itera.Current.GetAttribute("image", "");

					localization.AddLanguage(new Language(name, isDefault, image));
				}
			}

			_parameters.Localization = localization;

			
		}

		#endregion

		#region AUTHENTICATION

		private void SetAuthentication(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.Element);

			LoginSetting loginSetting = null;
			LogoutSetting logoutSetting = new LogoutSetting();
			UserInactivity userInactivity = null;
			Customer customer = null;

			string form = "", remoteErrorCodeMapper = "";

			//se presente, recupero l'attributo di abilitazione. Se non esiste, allora l'abilitazione è abilitata di default per mantenere la retrocompatibilità.
			var enabled = itIn.Current.GetAttribute("enabled", "").ConvertTo<bool?>(true) ?? false;

			while (itIn.MoveNext())
			{
				#region LOGIN

				if (itIn.Current.Name.EqualsIgnoreCase("login"))
				{
					var clientCode = itIn.Current.GetAttribute("client_code", "");
					var sourceType = itIn.Current.GetAttribute("source_type", "");
					var positions = new List<string>();

					XPathNodeIterator iteraPositions = itIn.Current.SelectChildren(XPathNodeType.Element);

					while (iteraPositions.MoveNext())
					{
						if (iteraPositions.Current.Name.EqualsIgnoreCase("position"))
						{
							var pos = iteraPositions.Current.InnerXml;

							if (string.IsNullOrEmpty(pos))
								continue;

							positions.Add(pos);
						}
					}

					loginSetting = new LoginSetting(clientCode, sourceType, positions);
				}

				#endregion

				#region LOGOUT

				else if (itIn.Current.Name.EqualsIgnoreCase("logout"))
				{
					//NOTE non gestito
				}

				#endregion

				#region USER INACTIVITY

				else if (itIn.Current.Name.EqualsIgnoreCase("user_inactivity"))
				{
					var timeout = itIn.Current.GetAttribute("timeout_minutes", "").ConvertTo<int>();

					userInactivity = new UserInactivity(timeout);
				}

				#endregion

				#region CUSTOMER

				else if (itIn.Current.Name.EqualsIgnoreCase("customer"))
				{
					var company = itIn.Current.GetAttribute("company", "");

					customer = new Customer(company);
				}

				#endregion

				#region Form

				else if (itIn.Current.Name.EqualsIgnoreCase("form"))
				{
					form = itIn.Current.InnerXml;
				}

				#endregion

				#region RemoteErrorCodeMapper

				else if (itIn.Current.Name.EqualsIgnoreCase("remote_error_code_mapper"))
				{
					remoteErrorCodeMapper = itIn.Current.InnerXml;
				}

				#endregion
			}

			_parameters.AuthenticationSetting = new AuthenticationSetting(enabled, loginSetting, logoutSetting, userInactivity, customer);

			if (!string.IsNullOrEmpty(form)) _parameters.AuthenticationSetting.Form = form;
			if (!string.IsNullOrEmpty(remoteErrorCodeMapper)) _parameters.AuthenticationSetting.RemoteErrorCodeMapper = remoteErrorCodeMapper;
		}

		#endregion

		#region LAYOUT

		private void SetLayout(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.All);

			var layoutSectionList = new List<LayoutSettingSection>();

			while (itIn.MoveNext())
			{
				if (itIn.Current.Name.EqualsIgnoreCase("LayoutSection"))
				{
					LayoutSettingSection settingSection = new LayoutSettingSection();

					settingSection.Title = itIn.Current.GetAttribute("title", "");
					settingSection.Icon = itIn.Current.GetAttribute("icon", "");
					settingSection.Descr = itIn.Current.GetAttribute("descr", "");
					settingSection.Visible = itIn.Current.GetAttribute("visible", "");

					XPathNodeIterator itera = itIn.Current.SelectChildren(XPathNodeType.All);
					List<LayoutSetting> layoutList = new List<LayoutSetting>();

					while (itera.MoveNext())
					{
						if (itera.Current.Name.EqualsIgnoreCase("setting"))
						{
							LayoutSetting setting = new LayoutSetting();
							setting.Name = itera.Current.GetAttribute("name", "");
							setting.Title = itera.Current.GetAttribute("title", "");
							setting.Descr = itera.Current.GetAttribute("descr", "");
							setting.Type = itera.Current.GetAttribute("type", "");
							setting.Icon = itera.Current.GetAttribute("icon", "");
							setting.Default = itera.Current.GetAttribute("default", "");
							setting.Value = itera.Current.Value;

							layoutList.Add(setting);
						}
						
					}
					settingSection.Settings = layoutList;

					layoutSectionList.Add(settingSection);
				}

			}

			_parameters.Layout = layoutSectionList;

		}

		#endregion


		#region SETTINGS

		private void SetSettings(XPathNavigator ite)
		{
			XPathNodeIterator itIn = ite.SelectChildren(XPathNodeType.All);

			var settingSectionList = new List<SettingSection>();

			while (itIn.MoveNext())
			{
				if (itIn.Current.Name.EqualsIgnoreCase("SettingSection"))
				{
					SettingSection settingSection = new SettingSection();

					settingSection.Title = itIn.Current.GetAttribute("title", "");
					settingSection.Icon = itIn.Current.GetAttribute("icon", "");
					settingSection.Descr = itIn.Current.GetAttribute("descr", "");
					settingSection.Visible = itIn.Current.GetAttribute("visible", "");

					XPathNodeIterator itera = itIn.Current.SelectChildren(XPathNodeType.All);
					List<Setting> settingList = new List<Setting>();

					while (itera.MoveNext())
					{
						if (itera.Current.Name.EqualsIgnoreCase("setting"))
						{
							Setting setting = new Setting();
							setting.Name = itera.Current.GetAttribute("name", "");
							setting.Type = itera.Current.GetAttribute("type", "");
							setting.Default = itera.Current.GetAttribute("default", "");
							setting.Value = itera.Current.Value;

							settingList.Add(setting);
						}

					}
					settingSection.Settings = settingList;

					settingSectionList.Add(settingSection);
				}

			}

			_parameters.Setting = settingSectionList;

		}

		#endregion

	}
}