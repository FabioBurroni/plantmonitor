﻿using System.Collections.Generic;
using System.Globalization;
using Configuration.Settings.Global;
using Configuration.Settings.UI;

namespace Configuration
{
  /// <summary>
  /// Classe base che contiene i parametri di configurazione
  /// </summary>
  public class BaseConfigurationParameters
  {
    #region STANDARD

    #region DATE FORMAT

    /// <summary>
    /// Formato della data da utilizzare
    /// </summary>
    public string DateFormat { get; internal set; } = "hh:mm:ss dd:MM:yyyyy";

    #endregion

    #region WEIGHT FORMAT

    /// <summary>
    /// Formato del peso
    /// </summary>
    public string WeightFormat { get; internal set; } = "0.0000";

    #endregion

    #region NUMBER FORMAT INFO

    /// <summary>
    /// Formato della cultura
    /// </summary>
    public NumberFormatInfo NumberFormatInfo { get; set; }

    #endregion

    #region WINDOW SETTING

    /// <summary>
    /// Impostazioni della finestra principale
    /// </summary>
    public WindowSetting Window { get; internal set; }

    #endregion

    #region SETTING CONNESSIONE XML CLIENT

    /// <summary>
    /// Parametri di connessione di XmlClinet
    /// </summary>
    public ConnectionSetting XmlClientConnectionSetting { get; internal set; }

    #endregion

    #region SETTING CONNESSIONE XML SERVER

    /// <summary>
    /// Parametri di connessione di XmlServer
    /// </summary>
    public ConnectionSetting XmlServerConnectionSetting { get; internal set; }

    #endregion

    #region RUN APPLICATION AT WINDOWS STARTUP

    /// <summary>
    /// Indica se far partire l'applicazione all'avvio di Windows
    /// </summary>
    public bool RunApplicationAtWindowsStartup { get; internal set; }

    #endregion

  
    #region UPDATE SETTINGS

    /// <summary>
    /// Parametri di aggiornamento
    /// </summary>
    public UpdateSetting UpdateSetting { get; internal set; }

    #endregion

    #region PREAMBLE

    /// <summary>
    /// Settaggi per i preamboli
    /// </summary>
    public CommandPreambleSetting CommandPreambleSetting { get; internal set; }

    #endregion

    #region LOG

    /// <summary>
    /// Settaggi per il log
    /// </summary>
    public LoggingSetting LogSetting { get; internal set; }

    #endregion

    #region AUTHENTICATION SETTINGS

    public AuthenticationSetting AuthenticationSetting { get; internal set; }

    #endregion

    #region VIEWS

    public Views Views { get; internal set; }

    #endregion

    #region LOCALIZZAZIONE

    public Localization Localization { get; internal set; }

    #endregion

    #region LAYOUT

    public List<LayoutSettingSection> Layout { get; internal set; }

    #endregion

    #region SETTING

    public List<SettingSection> Setting { get; internal set; }

    #endregion

    #endregion
  }
}