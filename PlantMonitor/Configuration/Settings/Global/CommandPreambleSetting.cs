﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Exceptions;

namespace Configuration.Settings.Global
{
  public class CommandPreambleSetting
  {
    #region Fields

    private readonly Dictionary<string, CommandPreamble> _preambles = new Dictionary<string, CommandPreamble>(StringComparer.OrdinalIgnoreCase);

    #endregion

    #region Public Properties

    public string this[string type] => GetPreamble(type);

    public IList<CommandPreamble> CommandPreambles => _preambles.Values.ToArray();

    #endregion

    #region Internal Methods

    internal void AddPreamble(CommandPreamble commandPreamble)
    {
      if (commandPreamble == null)
        throw new ArgumentNullException(nameof(commandPreamble));

      if (_preambles.ContainsKey(commandPreamble.Type))
        throw new DuplicateKeyException($"Type='{commandPreamble.Type}' already inserted");

      _preambles.Add(commandPreamble.Type, commandPreamble);
    }

    #endregion

    #region Public Methods

    public string GetFirstPreamble()
    {
      return _preambles.FirstOrDefault().Value?.Preamble ?? "";
    }

    public string GetPreamble(string type)
    {
      if (string.IsNullOrEmpty(type))
        throw new ArgumentNullOrEmptyException(nameof(type));

      CommandPreamble preamble;
      return _preambles.TryGetValue(type, out preamble) ? preamble.Preamble : string.Empty;
    }

    #endregion
  }

  public class CommandPreamble
  {
    public string Type { get; }
    public string Preamble { get; }

    public CommandPreamble(string type, string preamble)
    {
      if (string.IsNullOrEmpty(type))
        throw new ArgumentNullOrEmptyException(nameof(type));

      if (string.IsNullOrEmpty(preamble))
        throw new ArgumentNullOrEmptyException(nameof(preamble));

      Type = type;
      Preamble = preamble;
    }
  }
}