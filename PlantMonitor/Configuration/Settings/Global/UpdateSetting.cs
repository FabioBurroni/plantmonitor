﻿using System;

namespace Configuration.Settings.Global
{
  public class UpdateSetting
  {
    public UpdateSetting(string feedUrl, int checkingIntervalMinutes, int remindMeLaterIntervalMinutes, bool installWithoutUserConfirm)
    {
      FeedUrl = feedUrl;
      CheckingIntervalMinutes = checkingIntervalMinutes;
      RemindMeLaterIntervalMinutes = remindMeLaterIntervalMinutes;
      InstallWithoutUserConfirm = installWithoutUserConfirm;
    }

    /// <summary>
    /// Indirizzo di connessione per gli aggiornamenti automatici
    /// </summary>
    public string FeedUrl { get; }

    /// <summary>
    /// Ogni quanti minuti si controlla la presenza di aggiornamenti
    /// </summary>
    public int CheckingIntervalMinutes { get; }

    public TimeSpan CheckingInterval => TimeSpan.FromMinutes(CheckingIntervalMinutes);

    /// <summary>
    /// Ogni quanti minuti si ricorda all'utente la presenza di aggiornamenti
    /// </summary>
    public int RemindMeLaterIntervalMinutes { get; }

    public TimeSpan RemindMeLaterInterval => TimeSpan.FromMinutes(RemindMeLaterIntervalMinutes);

    /// <summary>
    /// Se true si forza l'aggiornamento senza chiedere conferma all'utente
    /// </summary>
    public bool InstallWithoutUserConfirm { get; }
  }
}
