﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Exceptions;

namespace Configuration.Settings.Global
{
	public class AuthenticationSetting
	{
		public bool Enabled { get; }

		public LoginSetting LoginSetting { get; }

		public LogoutSetting LogoutSetting { get; }

		public UserInactivity UserInactivity { get; }

		public Customer Customer { get; }

		public string Form { get; internal set; } = "Authentication.GUI.UsernameAndPasswordForm";

		public string RemoteErrorCodeMapper { get; internal set; } = "Authentication.Default.RemoteErrorCodeMapper";

		internal AuthenticationSetting(bool enabled, LoginSetting loginSetting, LogoutSetting logoutSetting, UserInactivity userInactivity, Customer customer)
		{
			if (enabled && loginSetting == null)
				throw new ArgumentNullException(nameof(loginSetting));

			if (enabled && logoutSetting == null)
				throw new ArgumentNullException(nameof(logoutSetting));

			if (enabled && userInactivity == null)
				throw new ArgumentNullException(nameof(userInactivity));

			if (enabled && customer == null)
				throw new ArgumentNullException(nameof(customer));

			Enabled = enabled;
			LoginSetting = loginSetting;
			LogoutSetting = logoutSetting;
			UserInactivity = userInactivity;
			Customer = customer;
		}
	}

	public class LoginSetting
	{
		private readonly List<string> _positions;

		public string ClientCode { get; }

		public string SourceType { get; }

		public IList<string> Positions => _positions.ToList();

		internal LoginSetting(string clientCode, string sourceType, IEnumerable<string> positions)
		{
			if (string.IsNullOrEmpty(clientCode))
				throw new ArgumentNullOrEmptyException(nameof(clientCode));

			if (string.IsNullOrEmpty(sourceType))
				throw new ArgumentNullOrEmptyException(nameof(sourceType));

			_positions = positions != null ?
				new List<string>(positions.Distinct(StringComparer.OrdinalIgnoreCase).OrderBy(p => p, StringComparer.OrdinalIgnoreCase)) :
				new List<string>();

			ClientCode = clientCode;
			SourceType = sourceType;
		}
	}

	public class LogoutSetting
	{
		internal LogoutSetting()
		{
			//NOTE per adesso non è gestito
		}
	}

	public class UserInactivity
	{
		public int TimeoutMinutes { get; }

		internal UserInactivity(int timeout)
		{
			TimeoutMinutes = timeout;
		}
	}

	public class Customer
	{
		public string Company { get; }

		internal Customer(string company)
		{
			Company = company;
		}
	}

	public class ManageUsersSetting
	{
		public bool CanCreateUser { get; internal set; }
		public bool CanEditUser { get; internal set; }
		public bool CanEditUserPassword { get; internal set; }
		public bool CanDeleteUser { get; internal set; }

		internal ManageUsersSetting()
			: this(false, false, false, false)
		{

		}

		internal ManageUsersSetting(bool canCreateUser, bool canEditUser, bool canEditUserPassword, bool canDeleteUser)
		{
			CanCreateUser = canCreateUser;
			CanEditUser = canEditUser;
			CanEditUserPassword = canEditUserPassword;
			CanDeleteUser = canDeleteUser;
		}
	}
}