﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Comparer;
using Utilities.Exceptions;

namespace Configuration.Settings.UI
{
  public class Views
  {
    private readonly SortedList<int, ViewSection> _viewSections = new SortedList<int, ViewSection>(new DescendingComparer<int>());

    public int Count => _viewSections.Count;

    internal void Add(ViewSection viewSection)
    {
      if (viewSection == null)
        throw new ArgumentNullException(nameof(viewSection));

      if (_viewSections.ContainsKey(viewSection.Position))
        throw new DuplicateKeyException($"Key {viewSection.Position} already inserted");

      _viewSections.Add(viewSection.Position, viewSection);
    }

    public IList<ViewSection> GetAll()
    {
      return _viewSections.Values.ToArray();
    }
  }
}