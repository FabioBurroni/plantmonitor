﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Exceptions;

namespace Configuration.Settings.UI
{
  public class ViewSection
  {
    private readonly List<ControlSection> _controlsSection = new List<ControlSection>();

    public ViewSection(string name, string text, int position, int duration, string allowedPositions)
    {
      if (string.IsNullOrEmpty(name))
        throw new ArgumentNullOrEmptyException(nameof(name));

      Name = name;
      Text = text;
      Position = position;
      Duration = duration;

      var allowedPositionsA = !string.IsNullOrEmpty(allowedPositions) ? allowedPositions.Split(';') : null;

      if(allowedPositionsA!=null && allowedPositionsA.Length>0)
        AllowedPositionsL.AddRange(allowedPositionsA);

    }

    public string Name { get; private set; }
    public string Text { get; private set; }
    public int Position { get; private set; }
    public int Duration { get; private set; }
    public List<string> AllowedPositionsL { get; private set; } = new List<string>();

    public int Count => _controlsSection.Count;

    public void Add(ControlSection controlSection)
    {
      if (controlSection == null)
        throw new ArgumentNullException(nameof(controlSection));

      if (_controlsSection.Any(x=> x.Type == controlSection.Type))
        throw new DuplicateKeyException($"Key type {controlSection.Type} already inserted for {controlSection.Name} in {Name} view");

      _controlsSection.Add(controlSection);
    }

    public IList<ControlSection> GetAll()
    {
      return _controlsSection.ToArray();
    }


    public bool IsPositionAllowed(List<string> configurationPositions)
    {
      if (configurationPositions.Count == 0)
        return true;
      if(AllowedPositionsL.Count==0)
        return true;

      if (AllowedPositionsL.Any(ap => configurationPositions.Any(cp => cp.ToLower() == ap.ToLower())))
        return true;
      return false;
    }

  }
}