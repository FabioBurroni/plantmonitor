﻿using System.Collections.Generic;

namespace Configuration.Settings.UI
{
  public class LayoutSetting
  {
    public string Title = "";
    public string Name = "";
    public string Icon = "Settings";
    public string Type = "default";
    public string Descr = "";
    public string Value = "";
    public string Default = "";

  }
}