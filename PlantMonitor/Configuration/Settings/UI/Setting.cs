﻿using System.Collections.Generic;

namespace Configuration.Settings.UI
{
  public class Setting
  {
    public string Name = "";
    public string Type = "default";
    public string Value = "";
    public string Default = "";

  }
}