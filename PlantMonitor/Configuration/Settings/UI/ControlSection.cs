﻿using System;

namespace Configuration.Settings.UI
{
  public class ControlSection
  {
    public string Name { get; private set; }
    public string Type { get; private set; }
    public string Container { get; private set; }
    public string Path { get; private set; }

    public ControlSection(string name, string type, string container, string path)
    {
      if (string.IsNullOrEmpty(name))
        throw new ArgumentNullException(nameof(name)); 
      
      if (string.IsNullOrEmpty(type))
        throw new ArgumentNullException(nameof(type));

      if (string.IsNullOrEmpty(container))
        throw new ArgumentNullException(nameof(container));

      if (string.IsNullOrEmpty(path))
        throw new ArgumentNullException(nameof(path));

      Name = name;
      Type = type;
      Container = container;
      Path = path;
    }

  }
}