﻿using System.Collections.Generic;

namespace Configuration.Settings.UI
{
  public class LayoutSettingSection
  {
    public string Title = "";    
    public string Icon = "Settings";
    public string Descr = "";
    public string Visible = "";

    public List<LayoutSetting> Settings;
  }
}