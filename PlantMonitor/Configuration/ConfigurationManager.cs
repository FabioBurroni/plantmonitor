﻿using System;
using System.IO;
using System.Reflection;
using System.Xml.XPath;
using Utilities.Atomic;
using Utilities.Extensions;

namespace Configuration
{
  public static class ConfigurationManager
  {
    private static string MainConfFolderName = "conf";
    private static string ConfFolderName = "conf";
    //private const string ConfFileName = "conf.xml";
    private static string ConfApplicationFileName = "application.xml";
    private static string ConfPositionFileName = "position.xml";
    private static string ConfConnectionsFileName = "connections.xml";
    private static string ConfLayoutFileName = "layout.xml";
    private static string ConfSettingFileName = "setting.xml";
    private static string MainConfFileName = "main_conf.xml";

    private static readonly ThreadSafeSingleShotGuard ShotGuard = new ThreadSafeSingleShotGuard();

    /// <summary>
    /// Parametri di configurazione letti dal file
    /// </summary>
    public static ConfigurationParameters Parameters { get; } = new ConfigurationParameters();

    static ConfigurationManager()
    {

    }

    public static void LoadConfiguration()
    {
      if (!ShotGuard.CheckAndSetFirstCall)
        return;
      
      MainConfRead();

      ConfigurationReader reader = new ConfigurationReader(Parameters);
      reader.LoadConfiguration(GetFullConfigurationFilePath(ConfConnectionsFileName));
      reader.LoadConfiguration(GetFullConfigurationFilePath(ConfPositionFileName));
      reader.LoadConfiguration(GetFullConfigurationFilePath(ConfApplicationFileName));
      reader.LoadConfiguration(GetFullConfigurationFilePath(ConfLayoutFileName));
      reader.LoadConfiguration(GetFullConfigurationFilePath(ConfSettingFileName));
    }

    private static void MainConfRead()
    {
      string mainConfFilePath = GetFullMainConfigurationFilePath();
      if (!File.Exists(mainConfFilePath))
        throw new FileNotFoundException("Main Configuration file non found", mainConfFilePath);
      var _xDoc = new XPathDocument(mainConfFilePath);
      var _xNav = _xDoc.CreateNavigator();

      XPathNodeIterator ite = _xNav.Select("/configuration");
      if (ite.MoveNext())
      {
        XPathNodeIterator itera = ite.Current.SelectChildren(XPathNodeType.All);
        while (itera.MoveNext())
        {
          if (itera.Current.Name.EqualsIgnoreCase("folder"))
          {
            //ConfFolderName += Path.DirectorySeparatorChar + itera.Current.GetAttribute("name", "");
            ConfFolderName = itera.Current.GetAttribute("name", "");
          

          ConfPositionFileName = itera.Current.GetAttribute("position", "");
          ConfConnectionsFileName = itera.Current.GetAttribute("connections", "");
          ConfApplicationFileName = itera.Current.GetAttribute("application", "");
          ConfLayoutFileName = itera.Current.GetAttribute("layout", "");
          ConfSettingFileName = itera.Current.GetAttribute("setting", "");
          }
        }
      }
    }



    public static string AssemblyDirectory
    {
      get
      {
        string codeBase = Assembly.GetExecutingAssembly().CodeBase;
        UriBuilder uri = new UriBuilder(codeBase);
        string path = Uri.UnescapeDataString(uri.Path);
        return Path.GetDirectoryName(path);
      }
    }

    private static string GetRelativeConfigurationFilePath(string confFileName)
    {
      return Path.Combine(ConfFolderName, confFileName);
    }

    private static string GetFullConfigurationFilePath(string confFileName)
    {
      //return Path.Combine(Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName), GetRelativeConfigurationFilePath());
      return Path.Combine(AssemblyDirectory, GetRelativeConfigurationFilePath(confFileName));
    }

    #region MAIN CONF
    private static string GetRelativeMainConfigurationFilePath()
    {
      return Path.Combine(MainConfFolderName, MainConfFileName);
    }
    private static string GetFullMainConfigurationFilePath()
    {
      return Path.Combine(AssemblyDirectory, GetRelativeMainConfigurationFilePath());
    }

    #endregion

    #region Public Properties
    /// <summary>
    /// Application parent directory
    /// </summary>
    /// <returns>Main parent folder path in string format </returns>
    /// <example>C:\\Cassioli</example>
    public static string MainParentDirectory { get => Path.Combine(MainDirectory, ".."); }

    /// <summary>
    /// Application directory (Conf_custom\connections.xml)
    /// </summary>
    /// <returns>Main folder path as string </returns>
    /// <example>C:\\Cassioli\Client</example>
    public static string MainDirectory { get => AssemblyDirectory; }

    /// <summary>
    /// Main configuration directory (Conf_custom\connections.xml)
    /// </summary>
    /// <returns>Main configuration folder path as string </returns>
    /// <example>C:\\Cassioli\Client\Conf</example>
    public static string MainConfDirectory { get => Path.Combine(MainDirectory, MainConfFolderName); }

    /// <summary>
    /// Main configuration file
    /// </summary>
    /// <returns>Main configuration file path as </returns>
    /// <example>C:\\Cassioli\Client\Conf\main_conf.xml</example>
    public static string MainConfFile { get => GetFullMainConfigurationFilePath(); }

    /// <summary>
    /// Custom configuration directory
    /// </summary>
    /// <returns>Custom configuration folder path as string </returns>
    /// <example>C:\\Cassioli\Client\Conf_Custom</example>
    public static string AppConfDirectory { get => Path.Combine(MainDirectory, ConfFolderName); }

    /// <summary>
    /// Application file
    /// </summary>
    /// <returns>Application file path as string</returns>
    /// <example>C:\\Cassioli\Client\Conf_Custom\application.xml</example>
    public static string ApplicationFile { get => GetFullConfigurationFilePath(ConfApplicationFileName); }

    /// <summary>
    /// Connections file
    /// </summary>
    /// <returns>Connections file path as string</returns>
    /// <example>C:\\Cassioli\Client\Conf_Custom\connections.xml</example>
    public static string ConnectionsFile { get => GetFullConfigurationFilePath(ConfConnectionsFileName); }

    /// <summary>
    /// Conf Position file
    /// </summary>
    /// <returns> Configuration position file path as string </returns>
    /// <example>C:\\Cassioli\Client\Conf_Custom\position.xml</example>
    public static string PositionFile { get => GetFullConfigurationFilePath(ConfPositionFileName); }

    /// <summary>
    /// Conf Layout file
    /// </summary>
    /// <returns> Configuration position file path as string </returns>
    /// <example>C:\\Cassioli\Client\Conf_Custom\layout.xml</example>
    public static string LayoutFile { get => GetFullConfigurationFilePath(ConfLayoutFileName); }

    /// <summary>
    /// Conf Setting file
    /// </summary>
    /// <returns> Configuration position file path as string </returns>
    /// <example>C:\\Cassioli\Client\Conf_Custom\setting.xml</example>
    public static string SettingFile { get => GetFullConfigurationFilePath(ConfSettingFileName); }

    #endregion

  }
}