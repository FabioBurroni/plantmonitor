﻿using System;

namespace Model.Common.Attributes
{
  /// <inheritdoc />
	/// <summary>
	/// Attributo che definisce un metodo gestore di una notifica proveniente dal server
	/// </summary>
	[AttributeUsage(AttributeTargets.Method)]
  public sealed class XmlNotificationAttribute : Attribute
  {
    /// <summary>
    /// Permette di portare in primo piano il controllo che riceve la notifica
    /// </summary>
    public bool BringToFront { get; set; }
  }
}
