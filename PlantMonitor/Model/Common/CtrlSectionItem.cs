﻿using System;

namespace Model.Common
{
  public class CtrlSectionItem
	{
		#region Costructor

		public CtrlSectionItem(string controlName, string controlPath)
		{
			if (controlName == null)
				throw new ArgumentNullException(nameof(controlName));
						
			ControlName = controlName;

			ControlPath = controlPath;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Nome del controllo
		/// </summary>
		public string ControlName { get; }

		/// <summary>
		/// Path del controllo
		/// </summary>
		public string ControlPath { get; }

		public CtrlFunction Control { get; set; }

		#endregion


	}
}
