﻿using System;
using System.Globalization;

namespace Model.Common
{
  public class CurrentDate : ModelBase
  {

    private CultureInfo _ci = new CultureInfo("en-GB");

    private string _currentTimeString = "nan";
    public string CurrentTimeString
    {
      get
      {
        return _currentTimeString;
      }
      set
      {
        if (_currentTimeString != value)
        {
          _currentTimeString = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _currentDayString = "nan";
    public string CurrentDayString
    {
      get
      {
        return _currentDayString;
      }
      set
      {
        if (_currentDayString != value)
        {
          _currentDayString = value;
          NotifyPropertyChanged();
        }
      }
    }

    public DateTime CurrentDateTime
    {
      set
      {
        if (value != DateTime.MinValue && value != DateTime.MaxValue)
        {
          CurrentTimeString = value.ToString("HH:mm");
          CurrentDayString = _ci.DateTimeFormat.GetDayName(value.DayOfWeek) + " "
            + value.Day + " " + _ci.DateTimeFormat.GetMonthName(value.Month) + " "
            + value.ToString("yyyy");
        }
      }
    }



 
  }
}
