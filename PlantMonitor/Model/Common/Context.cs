﻿using Model.Custom.C180279;
using System;
using System.Collections.Generic;
using System.Linq;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlServer;

namespace Model.Common
{
  public sealed class Context
  {
    #region Istanza

    private static readonly Lazy<Context> Lazy = new Lazy<Context>(() => new Context());

    public static Context Instance => Lazy.Value;

    private Context()
    {
      CurrentDate = new CurrentDate();
    }

    #endregion

    #region XmlClient

    public IXmlClient XmlClient { get; set; }

    #endregion

    #region XmlServer

    public IXmlServer XmlServer { get; set; }

    #endregion

    #region Positions

    /// <summary>
    /// Posizione di riferimento per il client (ne caso in cui il client configurato per più posizioni, viene restituita la prima)
    /// </summary>
    public string Position => Positions.FirstOrDefault();

    /// <summary>
    /// Posizioni di riferimento per il client
    /// </summary>
    public IList<string> Positions { get; } = new List<string>();

    #endregion




    private ServerStatus _serverStatus = new ServerStatus();

    public ServerStatus ServerStatus
    {
      get { return _serverStatus; }
      set { _serverStatus = value; }
    }

    public string ServerIpAddress { get; set; } = "";
    public string LocalIpAddress { get; set; } = "";
    public string Version { get; set; } = "";

    #region
    public static CurrentDate CurrentDate { get; set; }

    #endregion

    public static C180279_ProdutionInfo ProductionInfoYesterday { get; set; } = new C180279_ProdutionInfo();
    public static C180279_ProdutionInfo ProductionInfoToday { get; set; } = new C180279_ProdutionInfo();



  }
}
