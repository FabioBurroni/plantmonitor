﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Common
{
  public interface ICommandResponse
  {
    /// <summary>
    /// Nome del comando
    /// </summary>
    string CommandName { get; }

    /// <summary>
    /// Parametri del cpmando
    /// </summary>
    IList<string> CommandParameters { get; }

    /// <summary>
    /// Modello della risposta
    /// </summary>
    IModel ResponseModel { get; }
  }
}
