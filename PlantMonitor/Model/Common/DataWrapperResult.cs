﻿
namespace Model.Common
{
  public class DataWrapperResult : ModelBase
  {
    #region Constructor

    public DataWrapperResult(object data = null, string result = null)
    {
      Data = data;
      Result = result;
    }

    #endregion

    #region Properties

    public object Data { get; internal set; }
    public string Result { get; internal set; }

    #endregion

    #region Public Methods

    public T GetData<T>() where T : class
    {
      return Data as T;
    }

    #endregion
  }
}
