﻿using XmlCommunicationManager.XmlClient.Event;

namespace Model.Common
{
  public interface ICommandManager
  {
    /// <summary>
    /// Pre-decodifica il messaggio e restituisce il suo CommandResponse
    /// </summary>
    ICommandResponse PreDecode(MsgEventArgs e);

    /// <summary>
    /// Decodifica il messaggio ricevuto con successo inserendo i dati nell'apposito modello
    /// </summary>
    ICommandResponse Decode(MsgEventArgs e);

    /// <summary>
    /// Decodifica il messaggio di errore inserendo i dati nell'apposito modello
    /// </summary>
    ICommandResponse Decode(ExceptionEventArgs e);
  }
}
