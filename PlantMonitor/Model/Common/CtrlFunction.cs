﻿using System.Windows.Controls;

namespace Model.Common
{
  public class CtrlFunction : UserControl
  {
    #region Public Properties

    public virtual bool IsSelected { get; set; }

    #endregion

    #region Public Methods

    public virtual void Closing()
    {
    }

    #endregion
  }
}
