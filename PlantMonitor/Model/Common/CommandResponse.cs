﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using XmlCommunicationManager.Common.Xml;

namespace Model.Common
{
  public class CommandResponse : ICommandResponse
  {
    #region Constructor

    protected CommandResponse()
    {
      //Inheritance
    }

    public CommandResponse(XmlCommand xmlCmd)
    : this(xmlCmd.commandMethodName, xmlCmd.commandMethodParameters)
    {

    }

    public CommandResponse(string commandName, IEnumerable<string> commandParameters)
      : this()
    {
      if (commandName == null)
        throw new ArgumentNullException(nameof(commandName));

      CommandName = commandName.TrimPreamble();
      CommandParameters = commandParameters.EmptyIfNull().ToArray();
    }

    #endregion

    #region ICommandResponse Members

    public string CommandName { get; protected set; }
    public IList<string> CommandParameters { get; protected set; }
    public IModel ResponseModel { get; internal set; }

    #endregion
  }
}
