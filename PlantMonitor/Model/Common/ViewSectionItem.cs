﻿using System;

namespace Model.Common
{
  public class ViewSectionItem
	{
		#region Fields

		private string _text;

		#endregion

		#region Costructor

		public ViewSectionItem(string name, CtrlSectionItem header, CtrlSectionItem main, CtrlSectionItem footer, int duration)
		{
			if (name == null)
				throw new ArgumentNullException(nameof(name));
			if (header == null)
				throw new ArgumentNullException(nameof(header));
			if (main == null)
				throw new ArgumentNullException(nameof(main));
			if (footer == null)
				throw new ArgumentNullException(nameof(footer));

			Name = name;

			Header = header;
			Main = main;
			Footer = footer;
			Duration = duration;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Codice dell'item
		/// </summary>
		public string Name { get; }

		public CtrlSectionItem Header { get; set; }

		public CtrlSectionItem Main { get; set; }

		public CtrlSectionItem Footer { get; set; }

		public int Duration { get; set; }
		#endregion

	}
}
