﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using XmlCommunicationManager.Authorization;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;

namespace Model.Common
{
  public class CommandManager : ICommandManager
  {
    #region CAMPI

    private readonly IXmlClient _xmlClient;
    private readonly string _commandHeader;

    #endregion

    #region COSTRUTTORE

    public CommandManager(IXmlClient xmlClient, string commandHeader = "")
    {
      if (xmlClient == null)
        throw new ArgumentNullException(nameof(xmlClient));

      _xmlClient = xmlClient;
      _xmlClient.OnException += _xmlClient_OnException;
      _commandHeader = commandHeader?.Trim() ?? "";
    }



    #endregion

    #region RICHIESTE

    #region INVIO COMANDO

    /// <summary>
    /// Invia il comando
    /// </summary>
    /// <param name="receiverInstance">Istanza di chi riceve il comando</param>
    /// <param name="cmd">Comando</param>
    /// <param name="advancedUserCredentials">Eventuali autorizzazione di utente</param>
    /// <param name="prependHeader">Indica se anteporre header</param>
    /// <param name="root">Radice di ricezione</param>
    protected void SendCommand(object receiverInstance, string cmd, ICredentials advancedUserCredentials = null, bool prependHeader = true, string root = "custom")
    {
      if (prependHeader)
        cmd = cmd.Insert(0, _commandHeader);

      var xmlCommand = _xmlClient.CreateXmlCommand(cmd, "cassioli", advancedUserCredentials);
      _xmlClient.ProcessXmlDoc(xmlCommand, root, receiverInstance);
    }

    #endregion

    #endregion

    #region RISPOSTE

    private void _xmlClient_OnException(object sender, ExceptionEventArgs e)
    {

    }


    #region DECODIFICA

    public virtual ICommandResponse PreDecode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //restituisco la risposta al comando
      return new CommandResponse(resp.xmlCommand);
    }

    public virtual ICommandResponse Decode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //recupero la risposta al comando
      var commandResponse = new CommandResponse(resp.xmlCommand);

      try
      {
        //invoco il metodo
        var respModel = (IModel)GetType().InvokeMethod(this, commandResponse.CommandName, new object[] { resp });

        //setto il dato
        commandResponse.ResponseModel = respModel;
      }
      catch (Exception ex)
      {
        Console.WriteLine($"{commandResponse.CommandName} => Exception={ex.Message}");
      }

      return commandResponse;
    }

    public virtual ICommandResponse Decode(ExceptionEventArgs e)
    {
      var xmlCmd = e.xmlCommand as XmlCommand;
      if (xmlCmd == null)
        return null;

      //restituisco la risposta al comando
      return new CommandResponse(xmlCmd);
    }

    #endregion

    #endregion

    #region UTILITÀ

    protected string CreateCommand(string command, params string[] parameters)
    {
      return CreateCommand(command, parameters.EmptyIfNull().ToList());
    }

    protected string CreateCommand(string command, IEnumerable<string> parameters)
    {
      if (parameters == null)
        throw new ArgumentNullException(nameof(parameters));

      return XmlCommunicationManager.Extensions.XmlCommandExtension.CreateXmlCommand(command, parameters);
    }

    #endregion
  }
}
