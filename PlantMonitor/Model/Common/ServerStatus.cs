﻿
namespace Model.Common
{
  public class ServerStatus:ModelBase
  {
    private bool _IsConnected = true;
    public bool IsConnected
    {
      get { return _IsConnected; }
      set
      {
        if (value != _IsConnected)
        {
          _IsConnected = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _LocalIpAddress;
    public string LocalIpAddress
    {
      get { return _LocalIpAddress; }
      set
      {
        if (value != _LocalIpAddress)
        {
          _LocalIpAddress = value;
          NotifyPropertyChanged();
        }
      }
    }



    private string _ServerIpAddress;
    public string ServerIpAddress
    {
      get { return _ServerIpAddress; }
      set
      {
        if (value != _ServerIpAddress)
        {
          _ServerIpAddress = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
