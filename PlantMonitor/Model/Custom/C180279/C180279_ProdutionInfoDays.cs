﻿using Model.Common;

namespace Model.Custom.C180279
{
  public class C180279_ProdutionInfoDays:ModelBase
  {

    private C180279_ProdutionInfo _Today = new C180279_ProdutionInfo();
    public C180279_ProdutionInfo Today
    {
      get { return _Today; }
      set
      {
        if (value != _Today)
        {
          _Today = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C180279_ProdutionInfo _Yesterday = new C180279_ProdutionInfo();
    public C180279_ProdutionInfo Yesterday
    {
      get { return _Yesterday; }
      set
      {
        if (value != _Yesterday)
        {
          _Yesterday = value;
          NotifyPropertyChanged();
        }
      }
    }

  }
}
