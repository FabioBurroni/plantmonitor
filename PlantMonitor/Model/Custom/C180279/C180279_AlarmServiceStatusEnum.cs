﻿
namespace Model.Custom.C180279
{
  public enum C180279_AlarmServiceStatusEnum
  {
    UNDEFINED = 0,
    OK = 1,
    SERVICE = 2,
    ALARM = 3
  }
}
