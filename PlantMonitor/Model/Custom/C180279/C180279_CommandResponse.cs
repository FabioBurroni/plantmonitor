﻿using Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using XmlCommunicationManager.Common.Xml;

namespace Model.Custom.C180279
{
  public class C180279_CommandResponse : CommandResponse
  {
    #region Constructor

    public C180279_CommandResponse(XmlCommand xmlCmd)
      : this(xmlCmd.commandMethodName, xmlCmd.commandMethodParameters)
    {

    }

    public C180279_CommandResponse(string commandName, IEnumerable<string> commandParameters)
    {
      if (commandName == null)
        throw new ArgumentNullException(nameof(commandName));

      //tolgo subito l'eventuale preambolo
      commandName = commandName.TrimPreamble();

      var parameters = commandParameters.EmptyIfNull().ToArray();

      #region Plugin

      //se il nome è 'Plugin_Command' allora il nome del metodo è il primo parametro
      if (commandName == "Plugin_Command")
        commandName = parameters.First();

      #endregion

      CommandName = commandName;
      CommandParameters = parameters.ToArray();
    }

    #endregion
  }
}
