﻿using Model.Common;

namespace Model.Custom.C180279
{
  public class C180279_ProdutionInfo:ModelBase
  {


    private int _NumPalletIn;
    public int NumPalletIn
    {
      get { return _NumPalletIn; }
      set
      {
        if (value != _NumPalletIn)
        {
          _NumPalletIn = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _NumPalletOut;
    public int NumPalletOut
    {
      get { return _NumPalletOut; }
      set
      {
        if (value != _NumPalletOut)
        {
          _NumPalletOut = value;
          NotifyPropertyChanged();
        }
      }
    }
    

  }
}
