﻿using Model.Common;

namespace Model.Custom.C180279
{
  public class C180279_MasterStatus:ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C180279_SatelliteStatusEnum _Status = C180279_SatelliteStatusEnum.PLAY;
    public C180279_SatelliteStatusEnum Status
    {
      get { return _Status; }
      set
      {
        if (value != _Status)
        {
          _Status = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsOk;
    public bool IsOk
    {
      get { return _IsOk; }
      set
      {
        if (value != _IsOk)
        {
          _IsOk = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsMaintenaceRequest;
    public bool IsMaintenaceRequest
    {
      get { return _IsMaintenaceRequest; }
      set
      {
        if (value != _IsMaintenaceRequest)
        {
          _IsMaintenaceRequest = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsAlarm;
    public bool IsAlarm
    {
      get { return _IsAlarm; }
      set
      {
        if (value != _IsAlarm)
        {
          _IsAlarm = value;
          NotifyPropertyChanged();
        }
        NotifyPropertyChanged("AlarmServiceStatus");
      }
    }

    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
        }
        NotifyPropertyChanged("AlarmServiceStatus");
      }
    }

    private bool _IsCharging;
    public bool IsCharging
    {
      get { return _IsCharging; }
      set
      {
        if (value != _IsCharging)
        {
          _IsCharging = value;
          NotifyPropertyChanged();
        }
      }
    }


    
    public C180279_AlarmServiceStatusEnum AlarmServiceStatus
    {
      get 
      { 
        if(IsAlarm)
          return C180279_AlarmServiceStatusEnum.ALARM; 
        else if (IsService)
          return C180279_AlarmServiceStatusEnum.SERVICE;
        else
          return C180279_AlarmServiceStatusEnum.OK;
      }
      
    }

    public void Update(C180279_MasterStatus newMaster)
    {
      this.Status = newMaster.Status;
      this.IsOk= newMaster.IsOk;
      this.IsMaintenaceRequest = newMaster.IsMaintenaceRequest;
      this.IsAlarm = newMaster.IsAlarm;
      this.IsService = newMaster.IsService;
      this.IsCharging = newMaster.IsCharging;
    }
  }
}
