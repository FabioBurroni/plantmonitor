﻿using Model.Common;

namespace Model.Custom.C180279
{
  public class C180279_SlaveStatus:ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C180279_SatelliteStatusEnum _Status=C180279_SatelliteStatusEnum.PLAY;
    public C180279_SatelliteStatusEnum Status
    {
      get { return _Status; }
      set
      {
        if (value != _Status)
        {
          _Status = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsOk;
    public bool IsOk
    {
      get { return _IsOk; }
      set
      {
        if (value != _IsOk)
        {
          _IsOk = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _BatteryLevel;
    public int BatteryLevel
    {
      get { return _BatteryLevel; }
      set
      {
        if (value != _BatteryLevel)
        {
          _BatteryLevel = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _Wifi;
    public bool Wifi
    {
      get { return _Wifi; }
      set
      {
        if (value != _Wifi)
        {
          _Wifi = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsAlarm;
    public bool IsAlarm
    {
      get { return _IsAlarm; }
      set
      {
        if (value != _IsAlarm)
        {
          _IsAlarm = value;
          NotifyPropertyChanged();
        }
        NotifyPropertyChanged("AlarmServiceStatus");
      }
    }

    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
        }
        NotifyPropertyChanged("AlarmServiceStatus");
      }
    }

    public C180279_AlarmServiceStatusEnum AlarmServiceStatus
    {
      get
      {
        if (IsAlarm)
          return C180279_AlarmServiceStatusEnum.ALARM;
        else if (IsService)
          return C180279_AlarmServiceStatusEnum.SERVICE;
        else
          return C180279_AlarmServiceStatusEnum.OK;
      }

    }

    public void Update(C180279_SlaveStatus newSlave)
    {
      this.BatteryLevel = newSlave.BatteryLevel;
      this.IsOk= newSlave.IsOk;
      this.Status= newSlave.Status;
      this.Wifi= newSlave.Wifi;
      this.IsAlarm = newSlave.IsAlarm;
      this.IsService = newSlave.IsService;
    }
  }
}
