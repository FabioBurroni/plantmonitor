﻿using Model.Common;

namespace Model.Custom.C200318_2
{
  public class C200318_Pallet : ModelBase
  {

    private string _palletCode = string.Empty;
    public string PalletCode 
    {
      get
      {
        return _palletCode;
      }
      set
      {
        if(value != _palletCode)
        {
          _palletCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _orderCode = string.Empty;
    public string OrderCode 
    {
      get
      {
        return _orderCode;
      }
      set
      {
        if (value != _orderCode)
        {
          _orderCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _articleCode = string.Empty;
    public string ArticleCode 
    {
      get
      {
        return _articleCode;
      }
      set
      {
        if (value != _articleCode)
        {
          _articleCode = value;
          NotifyPropertyChanged();
        }
      } 
    }

    private string _lotCode = string.Empty;
    public string LotCode 
    {
      get
      {
        return _lotCode;
      }
      set
      {
        if (value != _lotCode)
        {
          _lotCode = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _productQty = 0;
    public int ProductQty 
    {
      get
      {
        return _productQty;
      }
      set
      {
        if (value != _productQty)
        {
          _productQty = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _karinaQtyDelivered = 0;
    public int KarinaQtyDelivered 
    {
      get
      {
        return _karinaQtyDelivered;
      }
      set
      {
        if (value != _karinaQtyDelivered)
        {
          _karinaQtyDelivered = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _karinaQtyToTake = 0;
    public int KarinaQtyToTake 
    {
      get
      {
        return _karinaQtyToTake;
      }
      set
      { 
        if(value != _karinaQtyToTake)
        {
          _karinaQtyToTake = value;
          NotifyPropertyChanged();
        }
      }
    }

    private int _orderCmpReqestedQty = 0;
    public int OrderCmpRequestedQty 
    {
      get
      {
        return _orderCmpReqestedQty;
      }
      set
      {
        if (value != _orderCmpReqestedQty)
        {
          _orderCmpReqestedQty = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _seq = string.Empty;
    public string Seq 
    {
      get
      {
        return _seq;
      }
      set
      {
        if(value != _seq)
        {
          _seq = value;
          NotifyPropertyChanged();
        }
      }
    }

    private string _destinationMachine = string.Empty;
    public string DestinationMachine 
    {
      get
      {
        return _destinationMachine;
      }
      set
      {
        if (value != _destinationMachine)
        {
          _destinationMachine = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _isService = false;
    public bool IsService 
    {
      get
      {
        return _isService;
      }
      set
      { 
        if(value != _isService)
        {
          _isService = value;
          NotifyPropertyChanged();
        }
      }
    }
  }
}
