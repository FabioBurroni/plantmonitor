﻿
namespace Model.Custom.C200318_2
{
  public enum C200318_SatelliteStatusEnum
  {
    PLAY = 1,
    STOP = 2,
    ERROR = 3,
    RESTORE = 4,
    RESET = 5,
    CHARGING = 6,
    PARKING = 7,
    MANUAL = 8,
  }
}
