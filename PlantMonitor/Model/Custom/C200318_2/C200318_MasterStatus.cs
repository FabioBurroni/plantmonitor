﻿using Model.Common;

namespace Model.Custom.C200318_2
{
  public class C200318_MasterStatus : ModelBase
  {

    private string _Code;
    public string Code
    {
      get { return _Code; }
      set
      {
        if (value != _Code)
        {
          _Code = value;
          NotifyPropertyChanged();
        }
      }
    }


    private C200318_SatelliteStatusEnum _Status = C200318_SatelliteStatusEnum.PLAY;
    public C200318_SatelliteStatusEnum Status
    {
      get { return _Status; }
      set
      {
        if (value != _Status)
        {
          _Status = value;
          NotifyPropertyChanged();
        }
      }
    }


    private bool _IsOk;
    public bool IsOk
    {
      get { return _IsOk; }
      set
      {
        if (value != _IsOk)
        {
          _IsOk = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsMaintenaceRequest;
    public bool IsMaintenaceRequest
    {
      get { return _IsMaintenaceRequest; }
      set
      {
        if (value != _IsMaintenaceRequest)
        {
          _IsMaintenaceRequest = value;
          NotifyPropertyChanged();
        }
      }
    }

    private bool _IsAlarm;
    public bool IsAlarm
    {
      get { return _IsAlarm; }
      set
      {
        if (value != _IsAlarm)
        {
          _IsAlarm = value;
          NotifyPropertyChanged();
        }
        NotifyPropertyChanged("AlarmServiceStatus");
      }
    }

    private bool _IsService;
    public bool IsService
    {
      get { return _IsService; }
      set
      {
        if (value != _IsService)
        {
          _IsService = value;
          NotifyPropertyChanged();
        }
        NotifyPropertyChanged("AlarmServiceStatus");
      }
    }

    private bool _IsCharging;
    public bool IsCharging
    {
      get { return _IsCharging; }
      set
      {
        if (value != _IsCharging)
        {
          _IsCharging = value;
          NotifyPropertyChanged();
        }
      }
    }


    
    public C200318_AlarmServiceStatusEnum AlarmServiceStatus
    {
      get 
      { 
        if(IsAlarm)
          return C200318_AlarmServiceStatusEnum.ALARM; 
        else if (IsService)
          return C200318_AlarmServiceStatusEnum.SERVICE;
        else
          return C200318_AlarmServiceStatusEnum.OK;
      }
      
    }

    public void Update(C200318_MasterStatus newMaster)
    {
      this.Status = newMaster.Status;
      this.IsOk= newMaster.IsOk;
      this.IsMaintenaceRequest = newMaster.IsMaintenaceRequest;
      this.IsAlarm = newMaster.IsAlarm;
      this.IsService = newMaster.IsService;
      this.IsCharging = newMaster.IsCharging;
    }
  }
}
