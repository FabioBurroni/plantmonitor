﻿using System;
using System.Collections.Generic;
using Utilities.Extensions;
using XmlCommunicationManager.Common.Xml;
using XmlCommunicationManager.XmlClient;
using XmlCommunicationManager.XmlClient.Event;
using Model.Common;
using Utilities.Extensions.MoreLinq;
using XmlCommunicationManager.Authorization;

namespace Model.Custom.C200318
{
  public class C200318_CommandManager : CommandManager
  {
    #region COSTRUTTORE

    public C200318_CommandManager(IXmlClient xmlClient, string commandHeader) : base(xmlClient, commandHeader)
    {
    }

    #endregion

    #region PluginCommand
    public void Plugin_Command(object receiverInstance, params string[] parameters)
    {
      Plugin_Command(receiverInstance, parameters.EmptyIfNull());
    }
    public void Plugin_Command(object receiverInstance, IEnumerable<string> parameters, ICredentials advancedUserCredentials = null)
    {
      SendCommand(receiverInstance, CreateCommand("Plugin_Command", parameters), advancedUserCredentials);
    }

    #endregion

    #region MONITOR

    public void RM_Monitor_MasterStatus(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Monitor_MasterStatus");
      Plugin_Command(receiverInstance, parameters);

      //SendCommand(receiverInstance, CreateCommand("RM_Monitor_MasterStatus", parameters));
    }
    private IModel RM_Monitor_MasterStatus(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_MasterStatus> list = new List<C200318_MasterStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          C200318_MasterStatus master = new C200318_MasterStatus();
          master.Code = item.getFieldVal<string>(startIndex++);
          master.Status = item.getFieldVal<C200318_SatelliteStatusEnum>(startIndex++);
          master.IsOk = item.getFieldVal<bool>(startIndex++);
          master.IsMaintenaceRequest = item.getFieldVal<bool>(startIndex++);
          master.IsAlarm = item.getFieldVal<bool>(startIndex++);
          master.IsService = item.getFieldVal<bool>(startIndex++);
          master.IsCharging = item.getFieldVal<bool>(startIndex++);
          list.Add(master);
        }
      }
      return dwr;
    }


    public void RM_Monitor_SatelliteStatus(object receiverInstance)
    {
      var parameters = new List<string>();
      parameters.Add("RM_Monitor_SatelliteStatus");
      Plugin_Command(receiverInstance, parameters);

      //SendCommand(receiverInstance, CreateCommand("RM_Monitor_SatelliteStatus", parameters));
    }
    private IModel RM_Monitor_SatelliteStatus(XmlCommandResponse resp)
    {
      DataWrapperResult dwr = new DataWrapperResult();
      List<C200318_SlaveStatus> list = new List<C200318_SlaveStatus>();
      dwr.Data = list;
      if (resp.itemCount > 0)
      {
        for (int i = 0; i < resp.itemCount; i++)
        {
          var item = resp.Items[i];
          int startIndex = 1;
          C200318_SlaveStatus satellite = new C200318_SlaveStatus();
          satellite.Code = item.getFieldVal<string>(startIndex++);
          satellite.Status = item.getFieldVal<C200318_SatelliteStatusEnum>(startIndex++);
          satellite.IsOk = item.getFieldVal<bool>(startIndex++);
          satellite.BatteryLevel = item.getFieldVal<int>(startIndex++);
          satellite.Wifi = item.getFieldVal<bool>(startIndex++);
          satellite.IsAlarm = item.getFieldVal<bool>(startIndex++);
          satellite.IsService = item.getFieldVal<bool>(startIndex++);
          list.Add(satellite);
        }
      }
      return dwr;
    }



    //public void RM_Monitor_WarehouseStatus(object receiverInstance)
    //{
    //  var parameters = new List<string>();
    //  SendCommand(receiverInstance, CreateCommand("RM_Monitor_WarehouseStatus", parameters));
    //}
    //private IModel RM_Monitor_WarehouseStatus(XmlCommandResponse resp)
    //{
    //  DataWrapperResult dwr = new DataWrapperResult();
    //  List<C200318_FloorStatus> list = new List<C200318_FloorStatus>();
    //  dwr.Data = list;
    //  int itemNumber = 0;
    //  if (resp.itemCount > 0)
    //  {
    //    int numFloor = resp.Items[itemNumber++].getFieldVal<int>(1);
    //    for (int i = 0; i < numFloor; i++)
    //    {
    //      int startIndex = 1;
    //      C200318_FloorStatus fs = new C200318_FloorStatus();
    //      fs.FloorNumber = resp.Items[itemNumber].getFieldVal<int>(startIndex++);
    //      fs.Capacity = resp.Items[itemNumber].getFieldVal<int>(startIndex++);
    //      fs.NumPallet = resp.Items[itemNumber].getFieldVal<int>(startIndex++);
    //      fs.NumHalfFullCell = resp.Items[itemNumber].getFieldVal<int>(startIndex++);
    //      fs.NumBlocked = resp.Items[itemNumber++].getFieldVal<int>(startIndex++);
    //      list.Add(fs);
    //    }
    //  }
    //  return dwr;
    //}
    //public void C200318_Monitor_ProductionInfo(object receiverInstance)
    //{
    //  var parameters = new List<string>();
    //  SendCommand(receiverInstance, CreateCommand("C200318_Monitor_ProductionInfo", parameters));

    //}
    //private IModel C200318_Monitor_ProductionInfo(XmlCommandResponse resp)
    //{
    //  DataWrapperResult dwr = new DataWrapperResult();
    //  C200318_ProdutionInfoDays prodInfo = new C200318_ProdutionInfoDays();
    //  dwr.Data = prodInfo;

    //  if (resp.itemCount > 0)
    //  {
    //    int startIndex = 1;
    //    var item = resp.Items[0];
    //    prodInfo.Today.NumPalletIn = item.getFieldVal<int>(startIndex++);
    //    prodInfo.Yesterday.NumPalletIn = item.getFieldVal<int>(startIndex++);
    //    prodInfo.Today.NumPalletOut = item.getFieldVal<int>(startIndex++);
    //    prodInfo.Yesterday.NumPalletOut = item.getFieldVal<int>(startIndex++);
    //  }
    //  return dwr;
    //}



    #endregion
     
    #region DECODIFICA

    public override ICommandResponse PreDecode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //restituisco la risposta al comando
      return new C200318_CommandResponse(resp.xmlCommand);
    }

    public override ICommandResponse Decode(MsgEventArgs e)
    {
      XmlCommandResponse resp = e.iXmlStreamer as XmlCommandResponse;
      if (resp == null)
        return null;

      //recupero la risposta al comando
      var commandResponse = new C200318_CommandResponse(resp.xmlCommand);

      try
      {
        //invoco il metodo
        var respModel = (IModel)GetType().InvokeMethod(this, commandResponse.CommandName, new object[] { resp });

        //setto il dato
        commandResponse.ResponseModel = respModel;
      }
      catch (Exception ex)
      {
        Console.WriteLine($"{commandResponse.CommandName} => Exception={ex.Message}");
      }

      return commandResponse;
    }

    public override ICommandResponse Decode(ExceptionEventArgs e)
    {
      var xmlCmd = e.xmlCommand as XmlCommand;
      if (xmlCmd == null)
        return null;

      //restituisco la risposta al comando
      return new C200318_CommandResponse(xmlCmd);
    }

    #endregion


  }
}
