﻿using Configuration;
using System.Linq;
using Utilities.Extensions;

namespace Model.Custom.C200318
{
  public static class C200318_BatteryLevel
  {

    public static int Min
    {
      get 
      {
        var batterySettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Battery").FirstOrDefault();
        return batterySettings.Settings.Where(x => x.Name == "MinLevel").FirstOrDefault().Value.ConvertToInt();
      }
      
    }


    public static int MediumLevel
    {
      get
      {
        var batterySettings = ConfigurationManager.Parameters.Layout.Where(x => x.Title == "Battery").FirstOrDefault();
        return batterySettings.Settings.Where(x => x.Name == "MediumLevel").FirstOrDefault().Value.ConvertToInt();
      }

    }

  }
}
