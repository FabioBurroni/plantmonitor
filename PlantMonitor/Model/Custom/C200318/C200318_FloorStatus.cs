﻿using Model.Common;
using System;

namespace Model.Custom.C200318
{
  public class C200318_FloorStatus:ModelBase
  {
        
    private int _FloorNumber;
    public int FloorNumber
    {
      get { return _FloorNumber; }
      set
      {
        if (value != _FloorNumber)
        {
          _FloorNumber = value;
          NotifyPropertyChanged();
        }
      }
    }



    private int _Capacity;
    public int Capacity
    {
      get { return _Capacity; }
      set
      {
        if (value != _Capacity)
        {
          _Capacity = value;
          NotifyPropertyChanged();
          //NotifyPropertyChanged(FulnessPercentage);
        }
      }
    }

    private int _NumPallet;
    public int NumPallet
    {
      get { return _NumPallet; }
      set
      {
        if (value != _NumPallet)
        {
          _NumPallet = value;
          NotifyPropertyChanged();
          //NotifyPropertyChanged(FulnessPercentage);
        }
      }
    }


    private int _NumHalfFullCell;
    public int NumHalfFullCell
    {
      get { return _NumHalfFullCell; }
      set
      {
        if (value != _NumHalfFullCell)
        {
          _NumHalfFullCell = value;
          NotifyPropertyChanged();
        }
      }
    }


    private int _NumBlocked;
    public int NumBlocked
    {
      get { return _NumBlocked; }
      set
      {
        if (value != _NumBlocked)
        {
          _NumBlocked = value;
          NotifyPropertyChanged();
        }
      }
    }


    /// <summary>
    /// Percentuale di riempimento
    /// </summary>
  

    private int _FulnessPercentage;
    public int FulnessPercentage
    {
      get { return _FulnessPercentage; }
      set
      {
        if (value != _FulnessPercentage)
        {
          _FulnessPercentage = value;
          NotifyPropertyChanged();
        }
      }
    }



    public void Update(C200318_FloorStatus newFloorStatus)
    {
      Capacity = newFloorStatus.Capacity;
      NumPallet = newFloorStatus.NumPallet;
      NumHalfFullCell = newFloorStatus.NumHalfFullCell;
      NumBlocked= newFloorStatus.NumBlocked;

      if (Capacity > 0)
      {
        decimal res = (decimal)NumPallet / (decimal)Capacity;
        var res1 = (int)Math.Floor(res * 100);
        FulnessPercentage = res1;
      }
      else
        FulnessPercentage =0;
    }
  }
}
