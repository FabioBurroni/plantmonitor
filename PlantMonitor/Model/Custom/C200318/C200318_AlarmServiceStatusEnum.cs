﻿
namespace Model.Custom.C200318
{
  public enum C200318_AlarmServiceStatusEnum
  {
    UNDEFINED = 0,
    OK = 1,
    SERVICE = 2,
    ALARM = 3
  }
}
