﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using Configuration;
using Microsoft.Win32;
using Utilities;
using Utilities.Extensions;
using Utilities.Extensions.MoreLinq;
using XmlCommunicationManager.XmlClient;
using System.Net;
using Model.Common;
using Configuration.Settings.UI;
using System.Collections.Generic;
using View.Common;
using System.Timers;
using NAppUpdate.UI.WPF;

using Localization;

//...29/04/2022
using Authentication;
using XmlCommunicationManager.XmlServer;
using Utilities.Threading;
using Authentication.Default;

namespace PlantMonitor
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    #region Fields
    private readonly Context _context = Context.Instance; 
    private ConfigurationParameters _confParameters;

    public List<ViewSectionItem> ViewSectionItemList = new List<ViewSectionItem>();

    private int currentViewIndex;
    public System.Timers.Timer ViewSwitchTimer;

    #region 29/04/2022 Added for Authentication an update over Notifications
    private readonly Authentication.AuthenticationManager _authenticationManager = Authentication.AuthenticationManager.Instance;
    private LooperThread _looperThread;
    #endregion

    #endregion

    #region COSTRUTTORE
    public MainWindow()
    {
      InitializeComponent();
      try
      {
        InitAndStart();
      }
      catch (Exception ex)
      {
        Application.Current.Shutdown();
      }

    }
    #endregion

    #region INITIALIZATION

    private void InitAndStart()
    {
      #region Init
      Init_Configuration();
      Init_Position();
      Init_Language();
      Init_Window();
      Init_Setup();
      Init_XmlClient();
      Init_XmlServer();
      Context.Instance.ServerStatus.LocalIpAddress = GetIpAddress();
      Context.Instance.ServerStatus.ServerIpAddress = _confParameters.XmlClientConnectionSetting.IpAddress;
      Context.Instance.Version = GetVersion();
      #endregion

      #region Start
      Start_XmlClient();
      Start_XmlServer();
      #endregion

      //LoadViews from file
      LoadViewSectionItemList();

      // Load first view 
      UpdateActualViews(currentViewIndex);
      
      //Start timer to switch views
      SetViewSwitchTimer();

      Start_CheckForUpdates();

      //start authentication
      Start();
    }
    private void UpdateActualViews(int index)
    {
      if (index < ViewSectionItemList.Count())
      {
        var ctrl = ViewSectionItemList.ElementAt(index);
      
        if (ctrl != null)
        {
          gridHeader.Children.Clear();
          gridMain.Children.Clear();
          gridFooter.Children.Clear();

          gridHeader.Children.Add(ctrl.Header.Control);
          gridMain.Children.Add(ctrl.Main.Control);
          gridFooter.Children.Add(ctrl.Footer.Control);
        }
      }
    }

    private void SetViewSwitchTimer()
    {
      ViewSwitchTimer = new System.Timers.Timer();

      int duration = ViewSectionItemList.ElementAt(currentViewIndex).Duration;
      if (duration > 0)
      {
        ViewSwitchTimer.Interval = duration * 1000;
        ViewSwitchTimer.AutoReset = true;
        ViewSwitchTimer.Elapsed += ViewSwitchTimer_Elapsed;

        ViewSwitchTimer.Start();
      }
      else
      {
        // do nothing
      }
    }

    private void ViewSwitchTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
      //Increment view index
      currentViewIndex = (currentViewIndex + 1) % ViewSectionItemList.Count();

      //Update interval duration with new view duration
      int duration = ViewSectionItemList.ElementAt(currentViewIndex).Duration;
      if (duration > 0)
      {
        ViewSwitchTimer.Interval = duration * 1000;
      }
      else
      {
        ViewSwitchTimer.Stop();
      }

      //UpdateActualView
      Application.Current.Dispatcher.BeginInvoke((Action)(() => UpdateActualViews(currentViewIndex))); 
    }


    private void LoadViewSectionItemList()
    {
      // workaround to load View Assembly in memory...
      var force1 = typeof(CtrlBase).Assembly;

      // get previous read views configuration from xml
      Views views = _confParameters.Views;

      //get list of ViewSection 
      List<ViewSection> viewSectionL = views.GetAll().OrderBy(x => x.Position).ToList();

      //... scorriamo le varie ViewSection (ossia le pagine composte da Header + Main + Footer)
      foreach(ViewSection vS in viewSectionL)
      {
        // ... per ogni ViewSection dobbiamo costruire un ViewSectionItem
        // per farlo recuperiamo però header, main e footer dalla lista dei controlli dichiarati

        // lista dei controlli
        List<ControlSection> controlSectionL = vS.GetAll().ToList();

        // creiamo i controlli 

        // control section header crea control section item header
        ControlSection controlSectionHeader = controlSectionL.Where(x => x.Type == "header").First();
        CtrlSectionItem ctrlSectionItemHeader = new CtrlSectionItem(controlSectionHeader.Name, controlSectionHeader.Path);
        ctrlSectionItemHeader.Control = (CtrlFunction)Activator.CreateInstance(ctrlSectionItemHeader.ControlPath.GetTypeInAppDomain());

        // control section main crea control section item main
        ControlSection controlSectionMain = controlSectionL.Where(x => x.Type == "main").First();
        CtrlSectionItem ctrlSectionItemMain = new CtrlSectionItem(controlSectionMain.Name, controlSectionMain.Path);
        ctrlSectionItemMain.Control = (CtrlFunction)Activator.CreateInstance(ctrlSectionItemMain.ControlPath.GetTypeInAppDomain());

        // control section footer crea control section item footer
        ControlSection controlSectionFooter = controlSectionL.Where(x => x.Type == "footer").First();
        CtrlSectionItem ctrlSectionItemFooter = new CtrlSectionItem(controlSectionFooter.Name, controlSectionFooter.Path);
        ctrlSectionItemFooter.Control = (CtrlFunction)Activator.CreateInstance(ctrlSectionItemFooter.ControlPath.GetTypeInAppDomain());

        //creaimo la view section item
        ViewSectionItem viewSectionItem = new ViewSectionItem(vS.Name, ctrlSectionItemHeader, ctrlSectionItemMain, ctrlSectionItemFooter, vS.Duration);

        //aggiungiamo la view section
        ViewSectionItemList.Add(viewSectionItem);
      }

    }

    #region GetIpAddress
    private string GetIpAddress()
    {
      IPHostEntry host = default(IPHostEntry);
      string hostName = System.Environment.MachineName;
      host = Dns.GetHostEntry(hostName);
      foreach (var ip in host.AddressList)
      {
        if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
        {
          return Convert.ToString(ip);
        }
      }
      return "";
    }
    #endregion

    #region GetVersion
    private string GetVersion()
    {
      return Assembly.GetExecutingAssembly().GetName().Version.ToString();
    }
    #endregion

    #region Init_Position
    private void Init_Position()
    {
      var positions = ConfigurationManager.Parameters.AuthenticationSetting.LoginSetting.Positions;

      //rimuovo eventuali caratteri speciali e aggiungo tutto al conteto
      positions.Select(p => p.Replace("@", "").Trim()).Distinct(StringComparer.OrdinalIgnoreCase).ForEach(p => _context.Positions.Add(p));

    }
    #endregion

    /// <summary>
    /// Inizializza la configurazione
    /// </summary>
    private void Init_Configuration()
    {
      #region CONFIGURATION

      Logging.Message("InitConfiguration(): Avvio lettura file di configurazione");
      try
      {
        ConfigurationManager.LoadConfiguration();
        _confParameters = ConfigurationManager.Parameters;
      }
      catch (Exception ex)
      {
        Logging.Message($"InitConfiguration(): errore lettura file di configurazione => Exception={ex.Message}");
        Environment.Exit(0);
      }

      Logging.Message("InitConfiguration(): Fine lettura di file di configurazione");

      #endregion
    }

    #region WINDOW

    private void Init_Window()
    {
      var assembly = Assembly.GetExecutingAssembly().GetName();
      //Title = $"{assembly.Name} v{assembly.Version}";
      Title = $"MONITOR v{assembly.Version}";

      Height = Math.Max(600, _confParameters.Window.Height);
      Width = Math.Max(800, _confParameters.Window.Width);

      if (_confParameters.Window.CanMinimize && !_confParameters.Window.Resizable)
        ResizeMode = ResizeMode.CanMinimize;
      else if (_confParameters.Window.CanMinimize && _confParameters.Window.Resizable)
        ResizeMode = ResizeMode.CanResize;
      else if (!_confParameters.Window.CanMinimize && !_confParameters.Window.Resizable)
        ResizeMode = ResizeMode.NoResize;
      else if (!_confParameters.Window.CanMinimize && _confParameters.Window.Resizable)
        SourceInitialized += delegate { HideMinimizeAndMaximizeButtons(this); };

      WindowState = _confParameters.Window.Maximized ? WindowState.Maximized : WindowState.Normal;
    }

    // from winuser.h
    private const int GWL_STYLE = -16,
      WS_MAXIMIZEBOX = 0x10000,
      WS_MINIMIZEBOX = 0x20000;

    [DllImport("user32.dll")]
    private static extern int GetWindowLong(IntPtr hwnd, int index);

    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hwnd, int index, int value);

    internal static void HideMinimizeAndMaximizeButtons(Window window)
    {
      IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(window).Handle;
      var currentStyle = GetWindowLong(hwnd, GWL_STYLE);

      SetWindowLong(hwnd, GWL_STYLE, (currentStyle & ~WS_MAXIMIZEBOX & ~WS_MINIMIZEBOX));
    }

    #endregion


    #region SETUP

    private void Init_Setup()
    {
      //registro o deregistro l'applicazione per partire lo start up di Windows
      SetupLaunchAtWindowsStartup(_confParameters.RunApplicationAtWindowsStartup);
    }

    /// <summary> 
    /// true:registra l'applicazione allo startup; false deregistra l'applicazione 
    /// </summary>
    private static void SetupLaunchAtWindowsStartup(bool isChecked)
    {
      //eventualmente utilizzare LocalMachine se si vuole questo settaggio per tutti gli utenti
      using (var registryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true))
      {
        var assembly = Assembly.GetExecutingAssembly();
        string appName = assembly.GetName().Name;
        string location = System.IO.Path.GetDirectoryName(new Uri(assembly.GetName().CodeBase).LocalPath);
        if (registryKey != null)
        {
          if (isChecked)
          {
            registryKey.SetValue(appName, location);
          }
          else if (registryKey.GetValue(appName) != null)
          {
            registryKey.DeleteValue(appName);
          }
        }
      }
    }

    #endregion
       
    #endregion

    #region XML CLIENT

    private void Init_XmlClient()
    {
      _context.XmlClient = XmlClientCreator.CreateXmlClient(_confParameters.XmlClientConnectionSetting.Version.ConvertTo<XmlClientVersion>(),
        _confParameters.XmlClientConnectionSetting.IpAddress,
        _confParameters.XmlClientConnectionSetting.PortNumber,
        _confParameters.XmlClientConnectionSetting.TimeOut);
    }

    private void Start_XmlClient()
    {
      _context.XmlClient.OnException += XmlClient_OnException;
      _context.XmlClient.OnRetrieveReplyFromServer += XmlClient_OnRetrieveReplyFromServer;

      _context.XmlClient.Start();
    }

    private void Stop_XmlClient()
    {
      _context.XmlClient.OnException -= XmlClient_OnException;
      _context.XmlClient.OnRetrieveReplyFromServer -= XmlClient_OnRetrieveReplyFromServer;

      _context.XmlClient?.Dispose();
    }


    #endregion

    #region Xml Server
    private void Init_XmlServer()
    {
      _context.XmlServer = XmlServerCreator.CreateXmlServer(_confParameters.XmlServerConnectionSetting.Version.ConvertTo<XmlServerVersion>(),
        _confParameters.XmlServerConnectionSetting.IpAddress,
        _confParameters.XmlServerConnectionSetting.PortNumber);
    }

    private void Start_XmlServer()
    {
      _context.XmlServer.Start();
    }

    private void Stop_XmlServer()
    {

      _context.XmlServer?.Dispose();
    }
    #endregion


    #region EVENTI

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      //setto il supporto ai tablet
      //TabletSupport.ShowOnScreenKeyboard(this);

    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      ViewSwitchTimer?.Stop();
      ViewSwitchTimer?.Dispose();

      //...invoke closing on all custom controls
      var ctrlList = ViewSectionItemList.Select(vsi=>vsi.Main.Control).ToList().OfType<CtrlFunction>();
      foreach (var ctrl in ctrlList)
      {
        ctrl.Closing();
      }

      CloseWindow();
    }

    private void Window_Unloaded(object sender, RoutedEventArgs e)
    {

    }

    #endregion

    #region CLOSE

    private void CloseWindow()
    {
      Stop_CheckForUpdates();
      Stop_XmlClient();
      Stop_XmlServer();
      Stop();
    }

    private void Window_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
      if (this.WindowStyle == WindowStyle.None)
        this.WindowStyle = WindowStyle.SingleBorderWindow;
      else
        this.WindowStyle = WindowStyle.None;

      CheckForUpdatesNow();
    }

    #endregion

    #region COMUNICAZIONE

    #region EVENTI XML CLIENT
    private void XmlClient_OnRetrieveReplyFromServer(object sender, XmlCommunicationManager.XmlClient.Event.MsgEventArgs e)
    {
      //string commandMethodName = null;
      //string[] commandMethodParameters = null;

      //Model.Context.Instance.CommandManagerHms.PreDecode(e, out commandMethodName, out commandMethodParameters);
      //var m = this.GetType().GetMethod(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
      //if (m == null)
      //	return;


      //var data = Context.Instance.CommandManagerHms.Decode(e, out commandMethodName, out commandMethodParameters);
      //Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      //{
      //	Dispatch(commandMethodName, data, commandMethodParameters);
      //}));
    }

    private void XmlClient_OnException(object sender, XmlCommunicationManager.XmlClient.Event.ExceptionEventArgs e)
    {
      //Console.WriteLine(e.exception);
      //string commandMethodName = null;
      //string[] commandMethodParameters = null;
      //Context.Instance.CommandManagerHms.DecodeException(e, out commandMethodName, out commandMethodParameters);
      //var m = this.GetType().GetMethod(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
      //if (m == null)
      //	return;
      //Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
      //	{
      //		Dispatch(commandMethodName, null, commandMethodParameters);
      //	}));

    }

    //private void Dispatch(string commandMethodName, IDataStructure data, string[] commandMethodParameters)
    //{
    //	object[] parameters = new object[] { data, commandMethodParameters };
    //	var m = GetType().GetMethod(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance);

    //	try
    //	{
    //		if (m != null)
    //			m.Invoke(this, parameters);
    //	}
    //	catch (Exception ex)
    //	{
    //		Console.WriteLine(ex);
    //	}

    //	//      _type.InvokeMember(commandMethodName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod, null, this, parameters);
    //	//var m = _type.GetMethod(commandMethodName);

    //	//if(m!=null)
    //	//{
    //	//	m.Invoke(this, parameters);
    //	//}

    //}
    #endregion

    #region COMANDI

    //TODO

    #endregion

    #region RISPOSTE

    //TODO

    #endregion

    #endregion

    #region LANGUAGE
    private readonly CultureManager _cultureManager = CultureManager.Instance;

    /// <summary>
    /// Inizializza la lingua
    /// </summary>
    private void Init_Language()
    {
      _cultureManager.Culture = _confParameters.Localization.DefaultLanguage.Name;
      _cultureManager.CultureChanged += delegate { Translate(); };
            Translate();
    }
    private void Translate()
    {
      butCheckUpdatesText.Text = Localization.Localize.LocalizeDefaultString((string)butCheckUpdatesText.Tag);

    }
    #endregion

    #region UPDATE

    private CheckForUpdatesWindow _checkForUpdatesWindow;

    private void Start_CheckForUpdates()
    {
      if (_confParameters.UpdateSetting == null)
        return;

      var updateSetting = _confParameters.UpdateSetting;

      var feedUrl = updateSetting?.FeedUrl;
      var checkingInterval = updateSetting?.CheckingInterval ?? TimeSpan.Zero;
      var remindMeLaterInterval = updateSetting?.RemindMeLaterInterval ?? TimeSpan.Zero;
      var installWithoutUserConfirm = updateSetting?.InstallWithoutUserConfirm ?? false;

      _checkForUpdatesWindow = new CheckForUpdatesWindow
      {
        InstallWithoutUserConfirm = installWithoutUserConfirm,
        RemindMeLaterInterval = remindMeLaterInterval
      };

      try
      {
        var res = _checkForUpdatesWindow.Start(feedUrl, checkingInterval);
      }
      catch (Exception e)
      {
        //MessageBox.Show(e.Message);
        Logging.WriteToLog(e.Message);
      }
    }

    private void btnUpdates_Click(object sender, RoutedEventArgs e)
    {
      CheckForUpdatesNow();
    }

    private void gridFooter_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
    {
      BorderUpdates.Visibility = Visibility.Visible;
    }

    
    private void gridFooter_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    {
      BorderUpdates.Visibility = Visibility.Hidden;
    }

   

    private void Stop_CheckForUpdates()
    {
      _checkForUpdatesWindow?.Stop();
      _checkForUpdatesWindow?.Close();
    }
    private void CheckForUpdatesNow()
    {
      _checkForUpdatesWindow?.CheckForUpdatesNow();
    }
    #endregion


    //...********************************************29/04/2022****
    #region Authentication

    /// <summary>
    /// Questo booleano mi indica se è il primo tentativo di autenticazione che faccio.
    /// Questo perchè, in caso di logout dell'applicazione e poi login, devo mostrare l'interfaccia di autenticazione 
    /// senza usare quick login o eredità
    /// </summary>
    private bool _isFirstAuthentication;

    private void ExecuteAuthentication()
    {

      var xmlServerConf = _confParameters.XmlServerConnectionSetting;
      var xmlClientConf = _confParameters.XmlClientConnectionSetting;

      var loginParameters = new LoginParameters(_confParameters.AuthenticationSetting.LoginSetting.ClientCode, _confParameters.AuthenticationSetting.LoginSetting.SourceType.ConvertTo<SourceType>(),
        xmlServerConf.PortNumber, null, _confParameters.AuthenticationSetting.LoginSetting.Positions);

      var authResponse = new AuthenticationResponse();
      var remoteErrorCodeMapper = new RemoteErrorCodeMapper();

      StopAndCleanAuthentication();
      _authenticationManager.InactivityTimeoutMilliseconds = TimeSpan.FromMinutes(_confParameters.AuthenticationSetting.UserInactivity.TimeoutMinutes).TotalMilliseconds;
      _authenticationManager.Configure(_context.XmlClient, _context.XmlServer, loginParameters, authResponse, remoteErrorCodeMapper);

      var credentials = new Credentials("MPS", "MPS".Base64Encode());
      _authenticationManager.Start(credentials, true);

      if (!_isFirstAuthentication)
        _isFirstAuthentication = true;
    }

    private void StopAndCleanAuthentication()
    {
      _authenticationManager.StopAndClean();
    }

    private void SubscribeAuthenticationEvents()
    {
      _authenticationManager.CloseRequest += AuthenticationManagerOnCloseRequest;
      _authenticationManager.LoginSession += AuthenticationManagerOnLoginSession;
      _authenticationManager.LogoutSession += AuthenticationManagerOnLogoutSession;
    }

    private void UnsubscribeAuthenticationEvents()
    {
      _authenticationManager.CloseRequest -= AuthenticationManagerOnCloseRequest;
      _authenticationManager.LoginSession -= AuthenticationManagerOnLoginSession;
      _authenticationManager.LogoutSession -= AuthenticationManagerOnLogoutSession;
    }

    #endregion

    #region Authentication Events

    private void AuthenticationManagerOnCloseRequest(object sender, EventArgs eventArgs)
    {

    }

    private void AuthenticationManagerOnLoginSession(object sender, LoginSessionEventArgs e)
    {
      //è stato fatto un login, pulisco
      InvokeControlAction(delegate
      {
        StartRefresh();
      });
    }

    private void AuthenticationManagerOnLogoutSession(object sender, LogoutSessionEventArgs e)
    {
      InvokeControlAction(delegate
      {
        if (e.IsSessionTerminated)
        {
          ExecuteAuthentication();
        }
      });
    }

    private static void InvokeControlAction(Action action)
    {
      Application.Current.Dispatcher.BeginInvoke(action);
    }

    #endregion

    #region Autantication methods

    public void Start()
    {
      SubscribeAuthenticationEvents();
      ExecuteAuthentication();
    }

    public void Stop()
    {
      StopRefresh();
      StopAndCleanAuthentication();
    }

    #endregion

    #region Refresh
    private void StartRefresh()
    {
      _looperThread = new LooperThread(Refresh, TimeSpan.FromSeconds(1.0), true);
    }

    private void Refresh()
    {
      //
    }

    private void StopRefresh()
    {
      _looperThread?.Dispose();
      _looperThread = null;
    }
    #endregion
  }
}
